<?php
/*
Plugin Name: Physcode - Visual Composer Addon
Plugin URI: http://physcode.com
Description: Shortcode extend of Visual Composer
Version: 1.0.2
Author: Physcode(info@physcode.com)
Author URI: http://physcode.com
Copyright 2014-2015 physcode.com. All rights reserved.
*/

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	die( '-1' );
}

function Register_Physcode_Vc_Addon( $prefix, $args ) {
	if ( is_array( $args ) ) {
		for ( $i = 0; $i < count( $args ); $i ++ ) {
			if ( $args[$i] <> '' ) {
				add_shortcode( $args[$i], $prefix . '_shortcode_' . $args[$i] );
			}
		}
	}
}

if ( function_exists( 'vc_add_shortcode_param' ) ) {
	vc_add_shortcode_param( 'datepicker', 'travelwp_datepicker_field', WP_PLUGIN_URL . '/physc-vc-addon/js/datepicker.js' );
}

function travelwp_datepicker_field( $settings, $value ) {
	$value  = (string) $value;
	$output = '<div class="date_param_block">'
		. '<input name="' . esc_attr( $settings['param_name'] ) . '" class="vc_param_datepicker wpb_vc_param_value wpb-textinput ' .
		esc_attr( $settings['param_name'] ) . ' ' .
		esc_attr( $settings['type'] ) . '_field" type="text" value="' . esc_attr( $value ) . '" />'
		. '</div>';

	return $output;
}

function Physc_scriptInitAdmin() {
	wp_enqueue_script( 'jquery-ui-datepicker' );
	wp_enqueue_style( 'jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css' );

}

add_action( 'admin_enqueue_scripts', 'Physc_scriptInitAdmin' );


/**
 * Add multi select field
 */
if ( function_exists( 'vc_add_shortcode_param' ) ) {
	vc_add_shortcode_param( 'multi_dropdown', 'Physc_multi_dropdown_settings_field' );
}

function Physc_multi_dropdown_settings_field( $settings, $value ) {
	if ( !is_array( $value ) ) {
		$value = $value ? $value : '';
		$value = array_filter( explode( ',', $value ) );
	}
	$output = '<div class="multi_dropdown_block">';

	$output .= '<select name="' . esc_attr( $settings['param_name'] ) . '" class="wpb_vc_param_value wpb-input wpb-select ' . esc_attr( $settings['param_name'] ) . ' ' .
		esc_attr( $settings['type'] ) . '_field" multiple="multiple">';

	if ( !empty( $settings['value'] ) ) {
		foreach ( $settings['value'] as $index => $data ) {
			if ( is_numeric( $index ) && ( is_string( $data ) || is_numeric( $data ) ) ) {
				$option_label = $data;
				$option_value = $data;
			} elseif ( is_numeric( $index ) && is_array( $data ) ) {
				$option_label = isset( $data['label'] ) ? $data['label'] : array_pop( $data );
				$option_value = isset( $data['value'] ) ? $data['value'] : array_pop( $data );
			} else {
				$option_value = $data;
				$option_label = $index;
			}
			$selected            = '';
			$option_value_string = (string) $option_value;
			$value_string        = $value;
			if ( '' !== $value && in_array( $option_value_string, $value_string ) ) {
				$selected = ' selected="selected"';
			}
			$option_class = str_replace( '#', 'hash-', $option_value );
			$output .= '<option class="' . esc_attr( $option_class ) . '" value="' . esc_attr( $option_value ) . '"' . $selected . '>'
				. htmlspecialchars( $option_label ) . '</option>';
		}
	}
	$output .= '</select>';
	$output .= '</div>';

	return $output;
}

//////////////////////////////////////////////////////////////////
// Remove extra P tags
//////////////////////////////////////////////////////////////////
function physcode_shortcodes_formatter( $content ) {
	$block = join( "|", array( "phys_custom_html" ) );
	// opening tag
	$rep = preg_replace( "/(<p>)?\[($block)(\s[^\]]+)?\](<\/p>|<br \/>)?/", "[$2$3]", $content );
	// closing tag
	$rep = preg_replace( "/(<p>)?\[\/($block)](<\/p>|<br \/>)/", "[/$2]", $rep );

	return $rep;
}

add_filter( 'the_content', 'physcode_shortcodes_formatter' );
add_filter( 'widget_text', 'physcode_shortcodes_formatter' );

require_once dirname( __FILE__ ) . '/importer/radium-importer.php'; //load admin theme data importer
