<?php
/**
 * Loop Booking
 *
 * @author : Physcode
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

$start_date              = get_post_meta( get_the_ID(), '_tour_start_date', true );
$end_date                = get_post_meta( get_the_ID(), '_tour_end_date', true );
$tour_days               = get_post_meta( get_the_ID(), '_price_each_day', true );
$number_ticket           = get_post_meta( get_the_ID(), '_tour_number_ticket', true );
$price_child             = get_price_for_child_on_tour( $product, '' );
$price_child_set_on_tour = get_post_meta( get_the_ID(), '_price_child', true ) ? get_post_meta( get_the_ID(), '_price_child', true ) : 0;
$currency                = get_woocommerce_currency_symbol( get_woocommerce_currency() );
$checkout_page_id        = get_option( 'woocommerce_checkout_page_id' );
if ( is_user_logged_in() ) {
	$user       = wp_get_current_user();
	$first_name = get_user_meta( $user->ID, 'billing_first_name', true );
	$last_name  = get_user_meta( $user->ID, 'billing_last_name', true );
	$email      = get_user_meta( $user->ID, 'billing_email', true );
	$phone      = get_user_meta( $user->ID, 'billing_phone', true );
}

$show            = true;
$show_first_name = apply_filters( 'tb_show_first_name', $show );
$show_last_name  = apply_filters( 'tb_show_last_name', $show );
$show_email      = apply_filters( 'tb_show_email', $show );
$show_phone      = apply_filters( 'tb_show_phone', $show );
$show_date_book  = apply_filters( 'tb_show_date_book', $show );

$tour         = wc_get_product( get_the_ID() );
$stock_status = $tour->get_stock_status();
$manage_stock = $tour->get_manage_stock();
$stock_qty    = $tour->get_stock_quantity();

if ( $product->get_price() != '' ) {
	?>

	<div class="booking">
		<?php
		if ( $stock_status == 'instock' ) {
			?>
			<div class="">
				<div class="form-block__title">
					<h4><?php _e( 'Book the tour', 'travel-booking' ) ?></h4>
				</div>
				<form id="tourBookingForm" method="POST" action="">
					<?php if ( $show_first_name ) { ?>
						<div class="">
							<input name="first_name" value="<?php echo isset( $first_name ) ? $first_name : '' ?>"
								   placeholder="<?php _e( 'First name', 'travel-booking' ) ?>" type="text">
						</div>
					<?php } ?>
					<?php if ( $show_last_name ) { ?>
						<div class="">
							<input name="last_name" value="<?php echo isset( $last_name ) ? $last_name : '' ?>"
								   placeholder="<?php _e( 'Last name', 'travel-booking' ) ?>" type="text">
						</div>
					<?php } ?>
					<?php if ( $show_email ) { ?>
						<div class="">
							<input name="email_tour" value="<?php echo isset( $email ) ? $email : '' ?>"
								   placeholder="<?php _e( 'Email', 'travel-booking' ) ?>" type="text">
						</div>
					<?php } ?>
					<?php if ( $show_phone ) { ?>
						<div class="">
							<input name="phone" value="<?php echo isset( $phone ) ? $phone : '' ?>"
								   placeholder="<?php _e( 'Phone', 'travel-booking' ) ?>" type="text">
						</div>
					<?php } ?>
					<?php if ( $show_date_book ) { ?>
						<div class="">
							<input type="text" name="date_book" value="" placeholder="<?php _e( 'Date Book', 'travel-booking' ) ?>">
						</div>
					<?php } ?>
					<!--for stock-->
					<?php
					if ( $manage_stock ) {
						echo '<div class="tour-stock-manage">';
						echo '<span>' . $stock_qty . '</span> ticket in stock';
						echo '</div>';
					}
					?>
					<div class="from-group">
						<div class="total_price_arrow">
							<?php
							if ( get_option( 'show_adults_children' ) ) {
								?>
								<div class="st_adults_children">
									<span class="label"><?php _e( 'Adults', 'travel-booking' ) ?></span>
									<div class="input-number-ticket">
										<input type="number" name="number_ticket" value="1" min="1"
											   max="<?php echo $number_ticket ?>"
											   placeholder="<?php get_option( 'show_adults_children' ) ? _e( 'Number ticket of Adults', 'travel-booking' ) : _e( 'Number ticket', 'travel-booking' ) ?>">
									</div>
									&times;
									<?php echo $currency ?>
									<span class="price_adults"><?php echo $product->get_price() ?></span>
									&#61;
									<span class="total_price_adults"><?php echo $currency . $product->get_price() ?></span>
								</div>

								<div class="st_adults_children">
									<span class="label"><?php _e( 'Children', 'travel-booking' ) ?></span>
									<div class="input-number-ticket">
										<input type="number" name="number_children" value="0" min="0"
											   max="<?php echo $number_ticket ?>"
											   placeholder="<?php _e( 'Number ticket of Children', 'travel-booking' ) ?>">
										<input type="hidden" name="price_child" value="<?php echo $price_child ?>">
										<input type="hidden" name="price_child_set_on_tour" value="<?php echo $price_child_set_on_tour ?>">
									</div>
									&times;
									<?php echo $currency ?>
									<span class="price_children"><?php echo $price_child ?></span>
									&#61;
									<span class="total_price_children">0</span>
								</div>
								<div>
									<?php _e( 'Total', 'travel-booking' ) ?>
									&#61;
									<span
										class="total_price_adults_children"><?php echo $currency . $product->get_price() ?></span>
								</div>
								<input type="hidden" name="price_children_percent"
									   value="<?php echo get_option( 'price_children' ) ?>">
								<?php
							} else {
								?>
								<span class="label"><?php _e( 'Total', 'travel-booking' ) ?></span>
								<div class="input-number-ticket">
									<input type="number" name="number_ticket" value="1" min="1"
										   max="<?php echo $manage_stock ? $stock_qty : $number_ticket ?>"
										   placeholder="<?php get_option( 'show_adults_children' ) ? _e( 'Number ticket of Adults', 'travel-booking' ) : _e( 'Number ticket', 'travel-booking' ) ?>">
								</div>
								&nbsp;
								&times;
								<span class="price_tour"><?php echo $currency . $product->get_price() ?></span>
								&#61;
								<span class="total_price"><?php echo $currency . $product->get_price() ?></span>
								<?php
							}
							?>
						</div>
					</div>
					<input type="hidden" name="tour_id" value="<?php echo get_the_ID() ?>">
					<input type="hidden" name="nonce" value="<?php echo wp_create_nonce( 'tb_booking_nonce_action' ) ?>">
					<div class="spinner">
						<div class="rect1"></div>
						<div class="rect2"></div>
						<div class="rect3"></div>
						<div class="rect4"></div>
						<div class="rect5"></div>
					</div>
					<input class="btn-booking btn" value="<?php _e( 'Booking now', 'travel-booking' ) ?>" type="submit">
					<input type="hidden" name="price" value="<?php echo $product->get_price(); ?>">
					<input type="hidden" name="start_date" value="<?php echo $start_date ?>">
					<input type="hidden" name="end_date" value="<?php echo $end_date ?>">
					<input type="hidden" name="tour_days" value="<?php echo htmlspecialchars( $tour_days ) ?>">
					<input type="hidden" name="day_book" value="">
					<input type="hidden" name="currency_symbol" value="<?php echo $currency ?>">
					<input type="hidden" name="url_home" value="<?php echo get_site_url() . '/' ?>">
					<?php
					if ( $_page_redirect_after_tour_booking = get_option( Tour_Settings_Tab_Phys::$_page_redirect_after_tour_booking ) ) {
						echo '<input type="hidden" name="checkout_url" value="' . get_page_link( $_page_redirect_after_tour_booking ) . '">';
					} else {
						echo '<input type="hidden" name="checkout_url" value="' . get_the_permalink( $checkout_page_id ) . '">';
					}
					?>

				</form>
			</div>
			<?php
		} else {
			echo '<p>Tour is Out of stock</p>';
		}
		?>

	</div>
	<?php
}
?>