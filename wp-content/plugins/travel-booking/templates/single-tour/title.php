<?php
/**
 * Product loop title
 *
 */

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<div class="title">
	<h1><?php the_title(); ?></h1>
</div>