<?php
/**
 * Content Single Tour
 *
 * @author : Physcode
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<?php

do_action( 'tour_booking_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form();

	return;
}
?>

<div id="tour-<?php the_ID(); ?>" <?php post_class( 'tb_single_tour' ); ?> class="tb_single_tour">

	<?php
	do_action( 'tour_booking_before_single_tour' );
	?>
	<div class="top_content_single row">
		<?php do_action( 'tour_booking_single_gallery' ); ?>
		<?php
		$date_finish_tour = get_post_meta( get_the_ID(), '_date_finish_tour', true );
		$date_now         = date( 'Y-m-d' );
		$show_booking     = false;
		if ( $date_finish_tour != 0 ) {
			$date_finish_tour_arr  = explode( '-', $date_finish_tour );
			$date_finish_tour_str  = $date_finish_tour_arr[0] . '-' . $date_finish_tour_arr[1] . '-' . $date_finish_tour_arr[2];
			$date_finish_tour_time = strtotime( $date_finish_tour_str );
			if ( strtotime( $date_now ) < $date_finish_tour_time ) {
				$show_booking = true;
			}
		}

		if ( $show_booking ) :
			?>
			<div class="summary entry-summary">
				<?php
				do_action( 'tour_booking_single_title' );
				do_action( 'tour_booking_single_ratting' );
				do_action( 'tour_booking_single_code' );
				do_action( 'tour_booking_single_price' );
				do_action( 'tour_booking_single_booking' );
				?>
			</div>
			<?php
		else:
			do_action( 'tour_booking_single_title' );
			do_action( 'tour_booking_single_price' );
		endif;

		?>
	</div>

	<div class="clear">
		<?php
		do_action( 'tour_booking_single_information' );
		do_action( 'tour_booking_single_related' );
		?>
	</div>

	<?php
	do_action( 'tour_booking_after_single_tour' );
	?>

</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'tour_booking_after_single_product' ); ?>
