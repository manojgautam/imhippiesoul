<?php
/**
 * Order Item Details frontend
 *
 * @author  physcode
 * @version 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
	return;
}

$currency = '';
if(TravelBookingPhyscode::$_version_woo < 3) {
	$currency = get_woocommerce_currency_symbol( $order->get_order_currency() );
} elseif (TravelBookingPhyscode::$_version_woo == 3) {
	$currency = get_woocommerce_currency_symbol( $order->get_currency() );
} else {
	$currency = get_woocommerce_currency_symbol( $order->get_currency() );
}

?>
<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_order_item_class', 'order_item', $item, $order ) ); ?>">
	<td class="product-name">
		<?php
		$is_visible        = $product && $product->is_visible();
		$product_permalink = apply_filters( 'woocommerce_order_item_permalink', $is_visible ? $product->get_permalink( $item ) : '', $item, $order );
		$date_booking      = wc_get_order_item_meta( $item_id, '_date_booking', true );
		$price_adults      = wc_get_order_item_meta( $item_id, '_price_adults', true );
		$number_children   = wc_get_order_item_meta( $item_id, '_number_children', true );
		$price_children    = wc_get_order_item_meta( $item_id, '_price_children', true );
		echo apply_filters( 'woocommerce_order_item_name', $product_permalink ? sprintf( '<a href="%s">%s</a>', $product_permalink, $item['name'] ) : $item['name'], $item, $is_visible );
		if ( $date_booking ) {
			echo '<p>' . __( 'Booking Date:', 'travel-booking' ) . $date_booking . '</p>';
			if ( isset( $price_adults ) ) {
				echo '<p>' . __( 'Adults', 'travel-booking' ) . '<span>(' . $currency . $price_adults . ')</span> &times; <span>' . $item['qty'] . '</span></p>';
			}
			if ( isset( $number_children ) ) {
				echo '<p>' . __( 'Children', 'travel-booking' ) . '<span>(' . $currency . $price_children . ')</span> &times; <span>' . $number_children . '</span></p>';
			}
		} else {
			echo apply_filters( 'woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf( '&times; %s', $item['qty'] ) . '</strong>', $item );
		}
		do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $order );

		if(TravelBookingPhyscode::$_version_woo < 3) {
			$order->display_item_meta( $item );
			$order->display_item_downloads( $item );
		} elseif (TravelBookingPhyscode::$_version_woo == 3) {
			wc_display_item_meta($item);
			wc_display_item_downloads( $item );
		} else {
			wc_display_item_meta($item);
			wc_display_item_downloads( $item );
		}

		do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $order );
		?>
	</td>
	<td class="product-total">
		<?php echo $order->get_formatted_line_subtotal( $item ); ?>
	</td>
</tr>
<?php if ( $show_purchase_note && $purchase_note ) : ?>
	<tr class="product-purchase-note">
		<td colspan="3"><?php echo wpautop( do_shortcode( wp_kses_post( $purchase_note ) ) ); ?></td>
	</tr>
<?php endif; ?>
