<?php
/**
 * Email Order Items
 *
 * This template can be overridden by copying it to yourtheme/travel-booking/emails/email-order-items.php.
 *
 *
 * @author        physcode
 * @version       1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$currency = '';
if(TravelBookingPhyscode::$_version_woo < 3) {
	$currency = get_woocommerce_currency_symbol( $order->get_order_currency() );
} elseif (TravelBookingPhyscode::$_version_woo == 3) {
	$currency = get_woocommerce_currency_symbol( $order->get_currency() );
} else {
	$currency = get_woocommerce_currency_symbol( $order->get_currency() );
}

foreach ( $items as $item_id => $item ) :
	$_product = apply_filters( 'woocommerce_order_item_product', $order->get_product_from_item( $item ), $item );
	$item_meta = new WC_Order_Item_Meta( $item, $_product );
	$date_booking = wc_get_order_item_meta( $item_id, '_date_booking', true );
	$price_adults = wc_get_order_item_meta( $item_id, '_price_adults', true );
	$number_children = wc_get_order_item_meta( $item_id, '_number_children', true );
	$price_children = wc_get_order_item_meta( $item_id, '_price_children', true );

	if ( apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
		?>
		<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_order_item_class', 'order_item', $item, $order ) ); ?>">
			<td class="td" style="text-align:left; vertical-align:middle; border: 1px solid #eee; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; word-wrap:break-word;"><?php

				// Show title/image etc
				if ( $show_image ) {
					echo apply_filters( 'woocommerce_order_item_thumbnail', '<div style="margin-bottom: 5px"><img src="' . ( $_product->get_image_id() ? current( wp_get_attachment_image_src( $_product->get_image_id(), 'thumbnail' ) ) : wc_placeholder_img_src() ) . '" alt="' . esc_attr__( 'Product Image', 'woocommerce' ) . '" height="' . esc_attr( $image_size[1] ) . '" width="' . esc_attr( $image_size[0] ) . '" style="vertical-align:middle; margin-right: 10px;" /></div>', $item );
				}

				// Product name
				if ( $date_booking ) {
					echo apply_filters( 'woocommerce_order_item_name', $item['name'], $item, false );
					echo '<p>' . __( 'Booking Date:', 'travel-booking' ) . $date_booking . '</p>';
					if ( isset( $price_adults ) ) {
						echo '<p>' . __( 'Adults', 'travel-booking' ) . '<span>(' . $currency . $price_adults . ')</span> &times; <span>' . $item['qty'] . '</span></p>';
					}
					if ( isset( $number_children ) ) {
						echo '<p>' . __( 'Children', 'travel-booking' ) . '<span>(' .$currency . $price_children . ')</span> &times; <span>' . $number_children . '</span></p>';
					}
				} else {
					echo apply_filters( 'woocommerce_order_item_name', $item['name'], $item, false );
					echo '&times;';
					echo apply_filters( 'woocommerce_email_order_item_quantity', $item['qty'], $item );
				}

				// SKU
				if ( $show_sku && is_object( $_product ) && $_product->get_sku() ) {
					echo ' (#' . $_product->get_sku() . ')';
				}

				// allow other plugins to add additional product information here
				do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $order, $plain_text );

				// Variation
				if ( ! empty( $item_meta->meta ) ) {
					echo '<br/><small>' . nl2br( $item_meta->display( true, true, '_', "\n" ) ) . '</small>';
				}

				// File URLs
				if ( $show_download_links ) {
					if(TravelBookingPhyscode::$_version_woo < 3) {
						$order->display_item_downloads( $item );
					} elseif (TravelBookingPhyscode::$_version_woo == 3) {
						wc_display_item_downloads( $item );
					} else {
						wc_display_item_downloads( $item );
					}
				}

				// allow other plugins to add additional product information here
				do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $order, $plain_text );

				?></td>
			<td class="td" style="text-align:left; vertical-align:middle; border: 1px solid #eee; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;"><?php echo $order->get_formatted_line_subtotal( $item ); ?></td>
		</tr>
		<?php
	}

	if ( $show_purchase_note && is_object( $_product ) && ( $purchase_note = get_post_meta( $_product->get_id(), '_purchase_note', true ) ) ) : ?>
		<tr>
			<td colspan="3" style="text-align:left; vertical-align:middle; border: 1px solid #eee; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;"><?php echo wpautop( do_shortcode( wp_kses_post( $purchase_note ) ) ); ?></td>
		</tr>
	<?php endif; ?>

<?php endforeach; ?>
