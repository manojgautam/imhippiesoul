<?php
/**
 * Single tour
 *
 * @author : Physcode
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
get_header( 'shop' ); ?>
<div class="container">
	<div class="row">
		<div class="site-main col-sm-9">
			<?php
			do_action( 'woocommerce_before_main_content' );
			?>

			<?php while ( have_posts() ) : the_post(); ?>
				<?php
				tb_get_file_template( 'content-single-tour.php' );
				?>
			<?php endwhile; ?>
			<?php
			do_action( 'woocommerce_after_main_content' );
			?>
		</div>
		<?php
		do_action( 'woocommerce_sidebar' );
		?>
	</div>
</div>
<?php
get_footer( 'shop' );
exit;
?>
