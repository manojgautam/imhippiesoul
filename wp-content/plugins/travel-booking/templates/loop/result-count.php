<?php
/**
 * Result Count
 *
 * Shows text: Showing x - x of x results.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/result-count.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $wp_query;

if ( ! woocommerce_products_will_display() )
	return;
?>
<p class="tour-result-count">
	<?php
	$paged    = max( 1, $wp_query->get( 'paged' ) );
	$per_page = $wp_query->get( 'posts_per_page' );
	$total    = $wp_query->found_posts;
	$first    = ( $per_page * $paged ) - $per_page + 1;
	$last     = min( $total, $wp_query->get( 'posts_per_page' ) * $paged );

	if ( $total <= $per_page || -1 === $per_page ) {
		printf( _n( 'Showing the single result', 'Showing all %d results', $total, 'travel-booking' ), $total );
	} else {
		printf( _nx( 'Showing the single result', 'Showing %1$d&ndash;%2$d of %3$d results', $total, '%1$d = first, %2$d = last, %3$d = total', 'travel-booking' ), $first, $last, $total );
	}
	?>
</p>
<form class="tour-ordering" method="get">
	<select name="orderby" class="orderby">
		<?php
		$orderby_selected        = isset( $_GET['orderby'] ) ? $_GET['orderby'] : '';
		$catalog_orderby_options = apply_filters(
			'tour_catalog_orderby', array(
				'menu_order' => __( 'Default sorting', 'travel-booking' ),
				'popularity' => __( 'Sort by popularity', 'travel-booking' ),
				'rating'     => __( 'Sort by average rating', 'travel-booking' ),
				'date'       => __( 'Sort by newness', 'travel-booking' ),
				'price'      => __( 'Sort by price: low to high', 'travel-booking' ),
				'price-desc' => __( 'Sort by price: high to low', 'travel-booking' )
			)
		);
		foreach ( $catalog_orderby_options as $id => $name ) :
			if ( $orderby_selected == esc_attr( $id ) ) {
				echo '<option value="' . esc_attr( $id ) . '"
									selected="' . $orderby_selected . '">' . esc_html( $name ) . '</option>';
			} else {
				echo '<option value="' . esc_attr( $id ) . '">' . esc_html( $name ) . '</option>';
			}
		endforeach;
		?>
	</select>
</form>
