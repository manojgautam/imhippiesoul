<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * @author : Physcode
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header();
global $wp_query;
$title = apply_filters( 'title_tours_page_default', 'Tours' );
if ( ! is_null( single_term_title( '', false ) ) ) {
	$title = single_term_title( '', false );
} elseif ( isset( get_queried_object()->ID ) ) {
	$title = get_the_title( get_queried_object()->ID );
} elseif ( get_option( Tour_Settings_Tab_Phys::$_tours_show_page_id ) ) {
	$title = get_the_title( get_option( Tour_Settings_Tab_Phys::$_tours_show_page_id ) );
}
?>
<div id="archive-tour" class="wrap">
	<div class="wrapper-tour">
		<div id="main-content" role="main">
			<div class="top_site_main">
				<div class="banner-wrapper container article_heading">
					<?php do_action( 'tour__breadcrumb' ); ?>
					<h1 class="page-title"><?php echo $title ?></h1>
				</div>
			</div>
			<section class="">
				<div class="container">
					<div class="site-main col-sm-9">
						<?php tb_get_file_template( 'loop/result-count.php' ) ?>

						<?php
						$tour_display_mode = 'grid';
						if ( get_option( 'tours_show_page_mode' ) ) {
							$tour_display_mode = get_option( 'tours_show_page_mode' );
						} ?>
						<ul class="tours-default wrapper-tours-slider <?php echo $tour_display_mode ?>">
							<?php if ( have_posts() ) : ?>

							<?php while ( have_posts() ) : the_post(); ?>

								<?php tb_get_file_template( 'content-tour.php', false ); ?>

							<?php endwhile; ?>
						</ul>
					<?php else: ?>

						<p class="woocommerce-info"><?php _e( 'No tours were found matching your selection.', 'travel-booking' ); ?></p>

					<?php endif; ?>
						<?php tb_get_file_template( 'loop/pagination.php' ) ?>
					</div>
				</div>
			</section>
		</div>
	</div>
	<div class="widget-area alignright col-sm-3">
		<?php
		dynamic_sidebar( 'sidebar-tour-phys' );
		?>
	</div>
</div>
<?php
get_footer();
exit;
?>
