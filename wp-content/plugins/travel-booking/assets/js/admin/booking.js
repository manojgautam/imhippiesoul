;(function ($) {
    var date_arr = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    $.fn.init_booking = function () {
        var tour_start_date = $('#tour_start_date');
        var tour_end_date = $('#tour_end_date');

        if (tour_start_date.length) {
            tour_start_date.datepicker({
                dateFormat: 'mm/dd/yy',
                // dateFormat: 'yy-mm-dd',
                maxDate: tour_end_date.val(),
                onClose: function (selectedDate) {
                    tour_end_date.datepicker("option", "minDate", selectedDate);
                    $.fn.set_date_finish_tour();
                }
            });
        }

        if (tour_end_date.length) {
            tour_end_date.datepicker({
                dateFormat: 'mm/dd/yy',
                // dateFormat: 'yy-mm-dd',
                minDate: tour_start_date.val(),
                onClose: function (selectedDate) {
                    tour_start_date.datepicker("option", "maxDate", selectedDate);
                    $.fn.set_date_finish_tour();
                }
            });
        }

        $('.price-for-each-day').find('.day').each(function (e) {
            if ($(this).is(':checked')) {

            } else {
                $(this).next().addClass('hidden');
                $(this).next().next().addClass('hidden');
            }
        });

        $('.day').on('click', function () {
            if ($(this).is(':checked')) {
                $(this).next().removeClass('hidden');
                $(this).next().next().removeClass('hidden');
            } else {
                $(this).next().addClass('hidden');
                $(this).next().next().addClass('hidden');
            }

            $.fn.save_price_each_day();
        });
        $('.price_day_input').on('keyup mouseup', function () {
            $.fn.save_price_each_day();
        });
        $('.price_day_input_children').on('keyup mouseup', function () {
            $.fn.save_price_each_day();
        });
    };

    $.fn.save_price_each_day = function () {
        var input_price_each_day = $('input[name=price_each_day]');
        var price_day_input_children = $('.price_day_input_children');
        var data_week = {};
        $('.price-for-each-day').find('.day').each(function (e) {
            if ($(this).is(':checked')) {
                data_week[$(this).val()] = {};
                if (!isNaN($(this).parent().find('.price_day_input').val())) {
                    data_week[$(this).val()].price = parseFloat($(this).parent().find('.price_day_input').val());
                    if (price_day_input_children.length > 0) {
                        data_week[$(this).val()].price_children = parseFloat($(this).parent().find('.price_day_input_children').val());
                    }
                } else {
                    data_week[$(this).val()].price = 0;
                }
            }
        });
        input_price_each_day.val(JSON.stringify(data_week));
        $.fn.set_date_finish_tour();
    };

    $.fn.set_date_finish_tour = function () {
        var tour_start_date = $('#tour_start_date');
        var tour_end_date = $('#tour_end_date');
        var price_each_day = $('input[name=price_each_day]');
        var date_finish_tour = $('input[name=date_finish_tour]');
        var date_now = new Date();
        var date_start_arr = tour_start_date.val().split('/');
        var date_start = new Date(date_start_arr[2], date_start_arr[0] - 1, date_start_arr[1], 0, 0, 0, 0); // y,m,d,h,i,s
        var date_end_arr = tour_end_date.val().split('/');
        var date_end = new Date(date_end_arr[2], date_end_arr[0] - 1, date_end_arr[1], 23, 59, 59, 0); // y,m,d,h,i,s

        if (tour_start_date.val().length != 0 && tour_end_date.val().length != 0) {
            date_finish_tour.val(0);
            console.log(date_now.getTime() - date_end.getTime());
            var date_pre = date_end;
            if (price_each_day.val().length > 2) {
                $.fn.find_date_finish(date_start, date_pre, date_finish_tour);
            } else {
                var date_result = date_end_arr[2] + '-' + (date_end_arr[0]) + '-' + date_end_arr[1];
                date_finish_tour.val(date_result);
            }
        }
    };

    $.fn.find_date_finish = function (date_start, date_pre, date_finish_tour) {
        var price_each_day = $('input[name=price_each_day]');
        var day_valid = JSON.parse(price_each_day.val());

        if (date_pre.getTime() >= date_start.getTime()) {
            var day_get = date_arr[date_pre.getDay()];
            console.log(day_get);
            if (typeof day_valid[day_get] != 'undefined') {
                var date_result = date_pre.getFullYear() + '-' + (date_pre.getMonth() + 1) + '-' + date_pre.getDate();
                date_finish_tour.val(date_result);
                console.log('okok');
            } else {
                date_pre.setDate(date_pre.getDate() - 1);
                console.log('run');
                $.fn.find_date_finish(date_start, date_pre, date_finish_tour);
            }
        }
        // else {
        //     var date_result = (date_now.getMonth() + 1) + '/' + date_now.getDate() + '/' + date_now.getFullYear();
        //     date_finish_tour.val(date_result);
        // }
    };

    $.fn.hidden_product_type_tour_on_list_product = function () {
        var j = 0;
        $('input[name=hidden_tour_phys]').each(function (i) {
            $(this).closest('tr').remove();
            j++;
        });
        if ($('input[name=active_tour_css]').length && j == 0) {
            $.fn.js_product_list_woo();
        }
    };

    $.fn.js_product_list_woo = function () {
        $('.wp-submenu').find('.wp-first-item').addClass('current');
        $('.wp-submenu').find('li.current').removeClass('current');
    };

    $.fn.js_custom_tab = function () {
        $('ul.tabs li').click(function(){

            var tab_id = $(this).attr('data-tab');
            console.log(tab_id);
            $('ul.tabs li').removeClass('current');
            $('.tab-content').removeClass('current');
            $(this).addClass('current');
            $("#"+tab_id).addClass('current');
        })
    };

    $.fn.active_menu_tour = function () {
        var $flag_set_current_tour = false;
        var $input_show_tour = $('input[name=menu_tour]');
        var $select_product_type = $('select[name=product-type]');
        if ($input_show_tour.length > 0) {
            $flag_set_current_tour = true;
        }
        if ($select_product_type.length > 0 && $select_product_type.val() == 'tour_phys') {
            $flag_set_current_tour = true;
        }

        if ($flag_set_current_tour) {
            var pattern = 'edit.php?post_type=product&product_type=tour_phys';
            $('li.current').removeClass('current');
            $('a[href$="' + pattern + '"]').parent().addClass('current')

			// change link product to link tour
			$('.subsubsub').find('li').find('a').each(function (i) {
				var $href = $(this).attr('href') + '&product_type=tour_phys';
				$(this).attr('href', $href);
			});
        }
    };

})(jQuery, 'tour-booking-phys');

jQuery(function ($) {
    'use strict';
    $.fn.init_booking();
    $.fn.hidden_product_type_tour_on_list_product();
    $.fn.js_custom_tab();
    $.fn.active_menu_tour();
});