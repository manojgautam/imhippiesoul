/**
 * Booking tour js
 *
 * @author physcode
 * @version 1.1.5
 */

;(function ($) {
	'use strict';
	var date_validate = [];
	var date_can_book = [];
	var date_arr = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	var tour_days = '';
	var date_book = '';
	var load_notify = false;

	$.fn.tour_booking_init = function () {
		$.fn.tour_booking();

		$('input[name=number_ticket]').on('keyup mouseup', function () {
			$.fn.update_tour_total_price($('input[name=date_book]').val());
		});

		$('input[name=number_children]').on('keyup mouseup', function () {
			$.fn.update_tour_total_price($('input[name=date_book]').val());
		});

		if (getCookie('info_user')) {
			var info_user = JSON.parse(getCookie('info_user'));
			$('input[name=first_name]').val(info_user.first_name);
			$('input[name=last_name]').val(info_user.last_name);
			$('input[name=email_tour]').val(info_user.email);
			$('input[name=phone]').val(info_user.phone);

			// for billing
			$('input[name=billing_first_name]').val(info_user.first_name);
			$('input[name=billing_last_name]').val(info_user.last_name);
			$('input[name=billing_email]').val(info_user.email);
			$('input[name=billing_phone]').val(info_user.phone);
		}
	};

	$.fn.tour_booking = function () {
		// get list date available
		var start_date_str = $('input[name=start_date]').val();
		var end_date_str = $('input[name=end_date]').val();
		var start_date = new Date(start_date_str);
		var end_date = new Date(end_date_str);
		var tour_days_arr = [];
		var date_now = new Date();

		if ($('input[name=tour_days]').length && $('input[name=tour_days]').val() != '') {
			tour_days = JSON.parse($('input[name=tour_days]').val());
			$.each(tour_days, function (i) {
				tour_days_arr.push(i);
			});
		}
		// console.log(tour_days_arr);

		var date_next = start_date;
		while (date_next.getTime() <= end_date.getTime()) {
			var date_next_str = ('0' + (date_next.getMonth() + 1)).slice(-2) + '/' + ('0' + date_next.getDate()).slice(-2) + '/' + date_next.getFullYear();
			// console.log(date_next_str);
			if (date_next.getTime() > date_now.getTime() || date_next.getTime() == date_now.getTime()) {
				if (tour_days_arr.length > 0) {
					if (tour_days_arr.indexOf(date_arr[date_next.getDay()]) != -1) {
						date_validate.push(date_next_str);
					}
				} else {
					date_validate.push(date_next_str);
				}
			}
			date_next.setDate(date_next.getDate() + 1);
		}

		$('input[name=date_book]').datepicker({
			dateFormat   : 'mm/dd/yy',
			beforeShowDay: date_available,
			onClose      : function (selectedDate) {
				if (selectedDate) {
					date_book = selectedDate;
					$.fn.update_tour_total_price(date_book);
				} else {
					date_book = '';
				}
			}
		});
	};

	function date_available(date) {
		var dmy = ("0" + (date.getMonth() + 1)).slice(-2) + "/" + ("0" + date.getDate()).slice(-2) + "/" + date.getFullYear();
		// console.log(dmy + ' : ' + ($.inArray(dmy, date_validate)));
		if ($.inArray(dmy, date_validate) != -1) {
			return [true, "date-available", "Available"];
		} else {
			return [false, "", "unAvailable"];
		}
	}

	$.fn.update_tour_total_price = function (date_book) {
		var price = $('input[name=price]');
		var qty = $('input[name=number_ticket]');
		var currency_symbol = $('input[name=currency_symbol]');
		var show_qty = $('.qty');
		var show_price = $('.price_tour');
		var show_total_price = $('.total_price');
		var total_price = 0;
		var input_number_children = $('input[name=number_children]');
		var input_price_child = $('input[name=price_child]');
		var input_price_child_set_on_tour = $('input[name=price_child_set_on_tour]');
		var input_price_children_percent = $('input[name=price_children_percent]');
		var qty_children = $('.qty_children');

		$('.total_price_arrow').find('span.qty').text(qty.val());
		if (typeof date_book != "undefined" && date_book != '') {
			var date_book_split_str = date_book.split('/');
			var date_book = new Date(date_book_split_str[2], date_book_split_str[0] - 1, date_book_split_str[1]);
			var day_number = date_book.getDay();
			var day_str = date_arr[day_number];
			$('input[name=day_book]').val(day_str);

			var price_day = 0;
			var price_tmp = price.val();
			var price_day_children = 0;
			var price_tmp_day_children = 0;

			if (typeof tour_days[day_str] != "undefined") {
				if (typeof tour_days[day_str]['price'] != "undefined" && !isNaN(tour_days[day_str]['price'])) {
					price_day = parseFloat(tour_days[day_str]['price']);
				} else if (!isNaN(tour_days[day_str])) {
					price_day = parseFloat(tour_days[day_str]);
				}
			}

			if (price_day > 0) {
				price_tmp = price_day;
			}

			if ($('.summary').find('ins').length > 0) {
				$('.summary').find('ins').find('.amount').text(currency_symbol.val() + price_tmp);
			} else {
				$('.summary').find('.price').find('.amount').text(currency_symbol.val() + price_tmp);
			}

			if (input_number_children.length > 0) {
				if (typeof tour_days[day_str] != "undefined" && !isNaN(tour_days[day_str]['price_children'])) {
					price_day_children = parseFloat(tour_days[day_str]['price_children']);
				}
				// set price children by day
				if (price_day_children > 0) {
					price_tmp_day_children = price_day_children;
				}

				// set price children on Tour
				if (price_tmp_day_children == 0) {
					price_tmp_day_children = input_price_child_set_on_tour.val();
					if (price_tmp_day_children == 0) {
						price_tmp_day_children = price_tmp * input_price_children_percent.val() / 100;
					}
				}

				var price_adults = parseFloat(qty.val() * price_tmp);
				var price_child = parseFloat(price_tmp_day_children * input_number_children.val());
				var total = price_child + price_adults;

				$('.price_adults').text(price_tmp);
				$('.price_children').text(price_tmp_day_children);
				$('.total_price_adults').text(currency_symbol.val() + price_adults.toFixed(2));
				$('.total_price_children').text(currency_symbol.val() + price_child.toFixed(2));
				$('.total_price_adults_children').text(currency_symbol.val() + total.toFixed(2));
			} else {
				show_price.text(currency_symbol.val() + price_tmp);
				show_total_price.text(currency_symbol.val() + (qty.val() * price_tmp).toFixed(2));
			}
		} else {
			if (input_number_children.length > 0) {
				var price_adults = parseFloat(qty.val() * price.val());
				var price_child = parseFloat(input_price_child.val() * input_number_children.val());
				var total = price_child + price_adults;

				qty_children.text(input_number_children.val());
				$('.total_price_adults').text(currency_symbol.val() + price_adults.toFixed(2));
				$('.total_price_children').text(currency_symbol.val() + price_child.toFixed(2));
				$('.total_price_adults_children').text(currency_symbol.val() + total.toFixed(2));
			} else {
				show_total_price.text(currency_symbol.val() + (qty.val() * price.val()).toFixed(2));
			}
		}
	};

	$.fn.tour_booking_submit = function () {
		$('.btn-booking').on('click', function (e) {
			e.preventDefault();
			var input_first_name = $('input[name=first_name]');
			var input_last_name = $('input[name=last_name]');
			var input_email = $('input[name=email_tour]');
			var input_phone = $('input[name=phone]');
			var input_date_book = $('input[name=date_book]');
			var input_qty = $('input[name=number_ticket]');
			var qty_tour = parseInt(input_qty.val());
			var input_number_children = $('input[name=number_children]');
			var tb_phys_ajax_url = $('input[name=url_home]').val();

			if (input_first_name.length > 0) {
				if (input_first_name.val().length == 0) {
					input_first_name.attr('placeholder', 'Enter first name.');
					input_first_name.addClass('error');
					return;
				}
			}

			if (input_last_name.length > 0) {
				if (input_last_name.val().length == 0) {
					input_last_name.attr('placeholder', 'Enter last name.');
					input_last_name.addClass('error');
					return;
				}
			}

			if (input_email.length > 0) {
				if (!checkValidateEmail(input_email.val())) {
					input_email.attr('placeholder', 'Email invalid.');
					input_email.addClass('error');
					input_email.val('');
					return;
				}
			}

			if (input_phone.length > 0) {
				if (input_phone.val().length == 0) {
					input_phone.attr('placeholder', 'Enter phone.');
					input_phone.addClass('error');
					return;
				}
			}

			if (input_date_book.length > 0) {
				if (input_date_book.val().length == 0) {
					input_date_book.attr('placeholder', 'Enter date.');
					input_date_book.addClass('error');
					return;
				}
			}

			if (isNaN(qty_tour) || qty_tour <= 0) {
				input_qty.val(1);
			}

			if (input_number_children.length > 0) {
				if (isNaN(input_number_children.val())) {
					input_number_children.val(0);
				}

				if (parseInt(input_qty.val()) + parseInt(input_number_children.val()) > parseInt(input_qty.attr('max'))) {
					$('.qty_error').remove();
					input_number_children.after('<span class="qty_error error">Only ' + input_qty.attr('max') + ' tickets</span>');
					return;
				}
			} else {
				if (parseInt(input_qty.val()) > parseInt(input_qty.attr('max'))) {
					$('.qty_error').remove();
					input_qty.after('<span class="qty_error error">Only ' + input_qty.attr('max') + ' tickets</span>');
					return;
				}
			}

			function checkValidateEmail(email) {
				var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				return regex.test(email);
			}

			// run ajax add to cart
			$('.spinner').show();
			if (!$(this).hasClass('disable')) {
				var data = {
					nonce        : $('input[name=nonce]').val(),
					tour_id      : $('input[name=tour_id]').val(),
					date_booking : input_date_book.val(),
					number_ticket: input_qty.val()
				};
				if (input_number_children.length > 0) {
					if (isNaN(parseInt(input_number_children.val()))) {
						data.number_children = 0;
					} else {
						data.number_children = input_number_children.val();
					}
				}
				$.ajax({
					url     : tb_phys_ajax_url + '?tb-ajax=add_tour_to_cart_phys',
					type    : 'post',
					data    : data,
					dataType: 'json',
					success : function (result) {
						if (result.status == 'success') {
							var user_info = {
								first_name: input_first_name.val(),
								last_name : input_last_name.val(),
								email     : input_email.val(),
								phone     : input_phone.val()
							};

							document.cookie = "info_user=" + JSON.stringify(user_info) + '; path=/';
							window.location = $('input[name=checkout_url]').val();
						} else {
							alert('errors booking. Try again');
						}
					}
				});
				$(this).addClass('disable');
			}
		});
	};

	$.fn.tour_orderby = function () {
		$('.tour-ordering').find('.orderby').on('change', function () {
			$('.tour-ordering').submit();
		});
	};

	$.fn.load_ajax_notify = function () {
		load_notify = false;
		var limit = $("input[name=notify_limit]").val();

		$.ajax({
			url     : tb_phys_ajax_url + "?tb-ajax=notify_new_order",
			type    : "post",
			data    : "limit=" + limit,
			dataType: "json",
			success : function (result) {
				if (result.status == "success") {
					$(".list-order-tour").html(result.html);
					load_notify = true;
				}
			}
		});
	};

	$.fn.load_notify_products_of_order = function () {
		var notify_available = $('input[name=notify_available]');
		if (notify_available.length) {
			load_notify = true;
			var refresh = $("input[name=notify_refresh]").val();
			if (refresh == "") {
				refresh = 10000;
			}

			setInterval(function () {
				if (load_notify) {
					$.fn.load_ajax_notify();
				}
			}, refresh);
		}
	};

	function getCookie(cname) {
		var name = cname + "=";
		var decodedCookie = decodeURIComponent(document.cookie);
		var ca = decodedCookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return "";
	}

})(jQuery, 'tour-booking-phys');

jQuery(function ($) {
	'use strict';
	$.fn.tour_booking_init();
	$.fn.tour_booking_submit();
	$.fn.tour_orderby();
	$.fn.load_notify_products_of_order();
});