<?php

/**
 * Plugin Name: Travel booking
 * Version: 1.2.4
 * Description: Option for Tour
 * Author: Physcode
 * Author URI: http://physcode.com/
 *  *
 * Text Domain: travel-booking
 * Domain Path: /languages/
 */
class TravelBookingPhyscode {
	private static $instance;
	public static $_version_woo = 0;
	private static $_og_title = '';

	public static function instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	public function __construct() {
		$this->plugin_defines();
		self::init();
	}

	protected function plugin_defines() {
		define( 'TOUR_BOOKING_PHYS_PATH', trailingslashit( WP_PLUGIN_DIR . '/' . str_replace( basename( __FILE__ ), "", plugin_basename( __FILE__ ) ) ) );
		define( 'TOUR_BOOKING_PHYS_URL', plugin_dir_url( __FILE__ ) );
		define( 'TB_PHYS_TEMPLATE_PATH_DEFAULT', TOUR_BOOKING_PHYS_PATH . 'templates' . DIRECTORY_SEPARATOR );
	}

	static function register_media_categories() {
		$args = array(
			'hierarchical'      => true,
			'labels'            => array(
				'name'          => __( 'Media Categories', 'travel-booking' ),
				'singular_name' => __( 'Media Category', 'travel-booking' ),
			),
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => false,
		);
		register_taxonomy( 'media_category', array( 'attachment' ), $args );
	}

	protected function init() {
		// check plugin Woocomerce installed
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		if ( ! is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
			add_action( 'admin_notices', array( $this, 'show_note_errors_install_plugin_woocomerce' ) );

			deactivate_plugins( plugin_basename( __FILE__ ) );
			if ( isset( $_GET['activate'] ) ) {
				unset( $_GET['activate'] );
			}

			return;
		}

		// check version plugin woo
		$path_plugin_woo                     = str_replace( 'travel-booking/', 'woocommerce/woocommerce.php', TOUR_BOOKING_PHYS_PATH );
		$data_plugin_woo                     = get_plugin_data( $path_plugin_woo, $markup = true, $translate = true );
		TravelBookingPhyscode::$_version_woo = (float) $data_plugin_woo['Version'];

		// Set up localisation.
		$this->load_plugin_textdomain();
		// add product type Tour
		include_once( 'inc/product-type-tour.php' );

		// shortcode
		//include_once( 'inc/shortcode/register_shortcode.php' );

		// add Tour Tab to Woo settings
		include_once( 'inc/tour_tab_settings.php' );

		// widget
		include_once( 'inc/widgets.php' );

		// sidebar
		include_once( 'inc/sidebar.php' );

		// get template path
		include_once( 'inc/tb-get-template.php' );

		// get template loader
		include_once( 'inc/override-theme-woo.php' );

		if ( is_admin() ) {
			// add tab Booking
			include_once( 'inc/tab-booking.php' );

			// show booking date on order
			include_once( 'inc/show-booking-date-order.php' );
		} else {
			$this->load_template_tour_frontend();

			// ajax
			include_once( 'inc/ajax_frontend.php' );
			add_action( 'template_redirect', array( $this, 'do_tb_ajax' ), 0 );

			include_once( 'inc/tb_query_phys.php' );

			include_once( 'inc/rewrite_url_tour.php' );

			include_once( 'inc/class-phys-cart-totals.php' );
		}

		self::init_hook();

		if ( is_admin() ) {
			add_action( 'admin_enqueue_scripts', array( $this, 'load_enqueue_scripts_on_admin' ) );
		} else {
			add_action( 'wp_enqueue_scripts', array( $this, 'load_enqueue_scripts_on_frontend' ) );
		}
	}

	protected static function init_hook() {
		// Register Taxonomy
		add_action( 'after_setup_theme', array( __CLASS__, 'register_taxonomies' ), 20 );
		add_action( 'after_setup_theme', array( __CLASS__, 'register_media_categories' ), 20 );
		add_filter( 'pre_get_document_title', array( __CLASS__, 'title_list_tour_phys' ), 999, 1 );
		add_action( 'template_redirect', array( __CLASS__, 'tour_title_setting' ), 1 );
	}

	public static function register_taxonomies() {
		$permalink_tour_category_base_option = get_option( Tour_Settings_Tab_Phys::$_permalink_tour_category_base );
		$labels                              = array(
			'name'              => __( 'Tour Types', 'travel-booking' ),
			'singular_name'     => __( 'Tour Type', 'travel-booking' ),
			'search_items'      => __( 'Search Tour Types', 'travel-booking' ),
			'all_items'         => __( 'All Tour Types', 'travel-booking' ),
			'parent_item'       => __( 'Parent Tour Type', 'travel-booking' ),
			'parent_item_colon' => __( 'Parent Tour Type:', 'travel-booking' ),
			'edit_item'         => __( 'Edit Tour Type', 'travel-booking' ),
			'update_item'       => __( 'Update Tour Type', 'travel-booking' ),
			'add_new_item'      => __( 'Add New Tour Type', 'travel-booking' ),
			'new_item_name'     => __( 'New Tour Type Name', 'travel-booking' ),
			'menu_name'         => __( 'Tour Type', 'travel-booking' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => $permalink_tour_category_base_option != '' ? $permalink_tour_category_base_option : 'tour-category' ),
		);

		register_taxonomy( 'tour_phys', array( 'product' ), $args );
		flush_rewrite_rules();
	}

	public static function tour_title_setting() {
		global $wp_query;
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

		$tour_show_page_id = get_option( Tour_Settings_Tab_Phys::$_tours_show_page_id );

		if ( $wp_query->get( 'is_tour' ) && $tour_show_page_id ) {
			$title           = get_the_title( $tour_show_page_id ) . ' - ' . get_bloginfo( 'name' );
			self::$_og_title = $title;
			$meta_array      = array(
				'og:title' => $title,
				'og:url'   => get_permalink( $tour_show_page_id )
			);

			if ( is_plugin_active( 'wordpress-seo/wp-seo.php' ) ) {
				add_filter( 'wpseo_opengraph_title', array( __CLASS__, 'change_og_title' ) );
				add_filter( 'wpseo_twitter_title', array( __CLASS__, 'change_og_title' ) );
				add_filter( 'wpseo_opengraph_url', array( __CLASS__, 'change_og_url' ) );
				add_filter( 'wpseo_twitter_url', array( __CLASS__, 'change_og_url' ) );
			} else {
				foreach ( $meta_array as $k => $v ) {
					echo '<meta property="', esc_attr( $k ), '" content="', esc_attr( $v ), '" />', "\n";
				}
			}
		} elseif ( is_single() ) {
			$meta_array = array(
				'og:image' => get_the_post_thumbnail_url( get_the_ID() ),
				'og:url'   => get_permalink( get_the_ID() )
			);

			foreach ( $meta_array as $k => $v ) {
				echo '<meta property="', esc_attr( $k ), '" content="', esc_attr( $v ), '" />', "\n";
			}
		}
	}

	public static function title_list_tour_phys( $title ) {
		global $wp_query;
		if ( $wp_query->get( 'is_tour' ) ) {
			$title_tours = get_the_title( get_option( Tour_Settings_Tab_Phys::$_tours_show_page_id ) ) ? get_the_title( get_option( Tour_Settings_Tab_Phys::$_tours_show_page_id ) ) : apply_filters( 'title_tour_page_default', 'Tours' );
			$title       = $title_tours . ' - ' . get_bloginfo( 'name' );
		}

		return $title;
	}

	public static function change_og_title( $title ) {
		return self::$_og_title;
	}

	public static function change_og_url( $url ) {
		$tour_show_page_id = get_option( Tour_Settings_Tab_Phys::$_tours_show_page_id );
		return get_permalink( $tour_show_page_id );
	}

	/**
	 * Load Localisation files.
	 *
	 * Note: the first-loaded translation file overrides any following ones if the same translation is present.
	 */
	public function load_plugin_textdomain() {
		$locale = apply_filters( 'plugin_locale', get_locale(), 'travel-booking' );
		load_textdomain( 'travel-booking', WP_LANG_DIR . '/travel-booking/travel-booking-' . $locale . '.mo' );
		load_plugin_textdomain( 'travel-booking', false, plugin_basename( dirname( __FILE__ ) ) . '/languages' );
	}

	function do_tb_ajax() {
		global $wp_query;
		if ( ! empty( $_GET['tb-ajax'] ) ) {
			$wp_query->set( 'tb-ajax', sanitize_text_field( $_GET['tb-ajax'] ) );
		}

		if ( $action = $wp_query->get( 'tb-ajax' ) ) {
			do_action( 'tb_ajax_' . sanitize_text_field( $action ) );
			die();
		}
	}

	function load_enqueue_scripts_on_admin() {
		// style
		wp_register_style( 'style-tour-booking-tab', TOUR_BOOKING_PHYS_URL . 'assets/css/admin/tab-booking.css' );
		wp_enqueue_style( 'style-tour-booking-tab' );

		// script
		wp_register_script( 'tour-booking-js', TOUR_BOOKING_PHYS_URL . 'assets/js/admin/booking.js' );
		wp_enqueue_script( 'tour-booking-js' );
	}

	function load_enqueue_scripts_on_frontend() {
		// style
		wp_enqueue_style( 'style-tour-booking-jq-ui-css', 'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css' );
		wp_enqueue_style( 'style-tour-booking-css', TOUR_BOOKING_PHYS_URL . 'assets/css/frontend/booking.css' );

		// script
		if ( ! wp_script_is( 'jquery' ) ) {
			wp_enqueue_script( 'tour-booking-jquery', 'https://code.jquery.com/jquery-3.1.1.min.js' );
		}

		wp_enqueue_script( 'tour-booking-jquery-ui', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js' );
		wp_enqueue_script( 'tour-booking-js-frontend', TOUR_BOOKING_PHYS_URL . 'assets/js/frontend/booking.js', array(), false, true );
	}

	function load_template_tour_frontend() {
		add_action( 'tour__breadcrumb', 'tour_booking_breadcrumb' );
		add_action( 'tour_booking_single_title', 'tour_booking_single_title' );
		add_action( 'tour_booking_single_ratting', 'tour_booking_single_ratting' );
		add_action( 'tour_booking_single_code', 'tour_booking_single_code' );
		add_action( 'tour_booking_single_price', 'tour_booking_single_price' );
		add_action( 'tour_booking_single_booking', 'tour_booking_single_booking' );
		add_action( 'tour_booking_single_gallery', 'tour_booking_single_gallery' );
		add_filter( 'tour_booking_default_product_tabs', 'tour_booking_default_product_tabs' );
		add_filter( 'tour_booking_default_product_tabs', 'tour_booking_sort_product_tabs', 99 );
		add_action( 'tour_booking_single_information', 'tour_booking_single_information' );
		add_action( 'tour_booking_single_related', 'tour_booking_single_related' );
	}

	public function show_note_errors_install_plugin_woocomerce() {
		?>
		<div class="notice notice-error is-dismissible">
			<p><?php _e( 'Please active plugin Woocomerce before active plugin Travel Booking', 'travel-booking' ); ?></p>
		</div>
		<?php
	}
}

$travelBookingPhyscode = TravelBookingPhyscode::instance();