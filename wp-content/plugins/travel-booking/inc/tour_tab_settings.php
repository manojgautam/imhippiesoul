<?php

/**
 *
 *  Tab settings for Tour
 *
 */
class Tour_Settings_Tab_Phys {
	public static $_tours_show_page_id = 'tours_show_page_id';
	public static $_permalink_tour_category_base = 'permalink_tour_category_base';
	public static $_page_redirect_after_tour_booking = 'page_redirect_after_tour_booking';

	public static function init() {
		add_action( 'init', array( __CLASS__, 'set_tours_options_with_lang_wmpl' ) );

		// create tab
		add_filter( 'woocommerce_settings_tabs_array', array( __CLASS__, 'add_settings_tab' ), 21 );
		// add fields
		add_action(
			'woocommerce_admin_field_multi_select_attribute', array(
				__CLASS__,
				'multi_select_attribute_woo'
			)
		);
		add_action( 'woocommerce_settings_tabs_tour_settings_phys', array( __CLASS__, 'settings_tab' ) );
		// save fields
		add_action( 'woocommerce_update_options_tour_settings_phys', array( __CLASS__, 'update_settings' ) );
	}

	public static function add_settings_tab( $settings_tabs ) {
		$settings_tabs['tour_settings_phys'] = __( 'Tours', 'travel-booking' );

		return $settings_tabs;
	}

	public static function settings_tab() {
		woocommerce_admin_fields( self::get_setting_tours() );
	}

	public static function get_setting_tours() {
		// create session
		self::create_section();
		self::get_attribute_of_woo_phys();
		global $current_section;
		switch ( $current_section ) {
			case 'search_tours' :
				$settings = array(
					'section_title'        => array(
						'title' => __( 'Settings search tours', 'travel-booking' ),
						'type'  => 'title',
						'desc'  => '',
						'id'    => 'catalog_options'
					),
					'search_by_attributes' => array(
						'title'   => __( 'Search by attribute', 'travel-booking' ),
						'desc'    => '<br/>' . __( 'Search tour by attribute set on Woocomerce', 'travel-booking' ),
						'id'      => 'tour_search_by_attributes',
						'type'    => 'multi_select_attribute',
						'default' => array(),
						//						'options' => self::get_attribute_of_woo_phys(),
					),
					'section_end'          => array(
						'type' => 'sectionend',
						'id'   => 'tour_options_phys'
					)
				);
				break;
			case 'permalink_tours' :
				$settings = array(
					'section_title'                      => array(
						'title' => __( 'Settings permalink tours', 'travel-booking' ),
						'type'  => 'title',
						'desc'  => '',
						'id'    => 'catalog_options'
					),
					self::$_permalink_tour_category_base => array(
						'title'   => __( 'Tour category base', 'travel-booking' ),
						'id'      => self::$_permalink_tour_category_base,
						'type'    => 'text',
						'default' => 'tour-category',
					),
					'section_end'                        => array(
						'type' => 'sectionend',
						'id'   => 'tour_options_phys'
					)
				);
				break;
			default:
				$settings = array(
					'section_title'                    => array(
						'title' => __( 'Tour Pages', 'travel-booking' ),
						'type'  => 'title',
						'desc'  => '',
						'id'    => 'catalog_options'
					),
					'page_tour'                        => array(
						'title'    => __( 'Tours Page', 'travel-booking' ),
						'desc'     => '<br/>',
						'id'       => self::$_tours_show_page_id,
						'type'     => 'single_select_page',
						'default'  => '',
						'class'    => 'wc-enhanced-select-nostd',
						'css'      => 'min-width:300px;',
						'desc_tip' => __( 'This sets the base page of your tour - this is where your tour archive will be.', 'travel-booking' ),
					),
					'tour_expire_on_list'              => array(
						'title'   => __( 'Tour Expired', 'travel-booking' ),
						'desc'    => '<br/>' . __( 'Show (Yes) or hide (No) tours Expire on list Tours', 'travel-booking' ),
						'id'      => 'tour_expire_on_list',
						'type'    => 'select',
						'default' => 'yes',
						'options' => array( 'yes' => 'Yes', 'no' => 'No' ),
						'class'   => 'wc-enhanced-select-nostd',
						'css'     => 'min-width:300px;',
					),
					'page_redirect_after_tour_booking' => array(
						'title'             => __( 'Redirect to page after Booking tour', 'travel-booking' ),
						'id'                => self::$_page_redirect_after_tour_booking,
						'type'              => 'single_select_page',
						'default'           => '',
						'option_none_value' => 6,
						'class'             => 'wc-enhanced-select-nostd',
						'css'               => 'min-width:300px;',
					),
					'google_api_key'                   => array(
						'title'    => __( 'Google API Key', 'travel-booking' ),
						'desc_tip' => __( 'Use show google map in tab Location of single tour', 'travel-booking' ),
						'desc'     => '<br/>' . __( 'How to get API Key https://developers.google.com/maps/documentation/javascript/get-api-key', 'travel-booking' ),
						'id'       => 'google_api_key',
						'default'  => '',
						'type'     => 'text',
						'css'      => 'min-width:350px;',
					),
					'show_adults_children'             => array(
						'title'   => __( 'Separate Ticket for Adult, Children', 'travel-booking' ),
						'desc'    => '<br/>',
						'id'      => 'show_adults_children',
						'type'    => 'select',
						'default' => 0,
						'options' => array( 1 => 'Yes', 0 => 'No' ),
						'class'   => 'wc-enhanced-select-nostd',
						'css'     => 'min-width:300px;',
					),
					'price_children'                   => array(
						'title'    => __( 'Price percent child/adult (%)', 'travel-booking' ),
						'desc'     => '<br/>',
						'desc_tip' => __( 'If you want set price children for each Tour, You can set set value Price Child on Tour when "Show number adults and children" enable', 'travel-booking' ),
						'id'       => 'price_children',
						'type'     => 'number',
						'default'  => '70',
 						'css'      => 'max-width:60px;',
					),
					'section_end'                      => array(
						'type' => 'sectionend',
						'id'   => 'tour_options_phys'
					)
				);
				break;
		}


		return apply_filters( 'tour_settings_metabox_phys', $settings );
	}

	public static function multi_select_attribute_woo( $value ) {
		$selections = (array) WC_Admin_Settings::get_option( $value['id'] );
		$attributes = self::get_attribute_of_woo_phys()
		?>
		<tr valign="top">
			<th scope="row" class="titledesc">
				<label for=""><?php echo esc_html( 'Attributes' ); ?></label>
			</th>
			<td class="forminp">
				<select multiple="multiple" name="<?php echo esc_attr( $value['id'] ); ?>[]" style="width:350px"
						data-placeholder="<?php esc_attr_e( 'Choose attributes&hellip;', 'travel-booking' ); ?>"
						title="<?php esc_attr_e( 'Attributes', 'travel-booking' ) ?>" class="wc-enhanced-select">
					<?php
					if ( ! empty( $attributes ) ) {
						foreach ( $attributes as $key => $val ) {
							echo '<option value="' . esc_attr( $key ) . '" ' . selected( in_array( $key, $selections ), true, false ) . '>' . $val . '</option>';
						}
					}
					?>
				</select>
				<a class="select_all button" href="#"><?php _e( 'Select all', 'travel-booking' ); ?></a>
				<a class="select_none button" href="#"><?php _e( 'Select none', 'travel-booking' ); ?></a>
			</td>
		</tr>
		<?php
	}

	public static function create_section() {
		$sections = array(
			''                => __( 'General', 'travel-booking' ),
			'search_tours'    => __( 'Search Tours', 'travel-booking' ),
			'permalink_tours' => __( 'Permalink Tours', 'travel-booking' ),
		);
		echo '<ul class="subsubsub">';
		global $current_section;
		$array_keys = array_keys( $sections );

		foreach ( $sections as $id => $label ) {
			echo '<li><a href="' . admin_url( 'admin.php?page=wc-settings&tab=tour_settings_phys&section=' . sanitize_title( $id ) ) . '" class="' . ( $current_section == $id ? 'current' : '' ) . '">' . $label . '</a> ' . ( end( $array_keys ) == $id ? '' : '|' ) . ' </li>';
		}

		echo '</ul><br class="clear" />';
	}

	public static function update_settings() {
		woocommerce_update_options( self::get_setting_tours() );
	}

	public static function get_attribute_of_woo_phys() {
		$taxonomies    = get_object_taxonomies( 'product', 'objects' );
		$attribute_arr = array();
		if ( empty( $taxonomies ) ) {
			return '';
		}

		foreach ( $taxonomies as $tax ) {
			$tax_name  = $tax->name;
			$tax_label = $tax->label;
			if ( 0 !== strpos( $tax_name, 'pa_' ) ) {
				continue;
			}
			if ( ! in_array( $tax_name, $attribute_arr ) ) {
				$attribute_arr[$tax_name] = $tax_label;
			}
		}


		return apply_filters( 'attribute_off_woo_phys', $attribute_arr );
	}

	public static function set_tours_options_with_lang_wmpl() {
		// check plugin WPML installed
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		if ( is_plugin_active( 'woocommerce-multilingual/wpml-woocommerce.php' ) && is_plugin_active('sitepress-multilingual-cms/sitepress.php') ) {
			global $sitepress;
			$default_lang = $sitepress->get_default_language();
			if ( $default_lang !== ICL_LANGUAGE_CODE ) {
				self::$_tours_show_page_id               = self::$_tours_show_page_id . '_' . ICL_LANGUAGE_CODE;
				self::$_page_redirect_after_tour_booking = self::$_page_redirect_after_tour_booking . '_' . ICL_LANGUAGE_CODE;
				self::$_permalink_tour_category_base     = self::$_permalink_tour_category_base . '_' . ICL_LANGUAGE_CODE;

				// Reregister taxonomy
				add_action( 'init', array( 'TravelBookingPhyscode', 'register_taxonomies' ), 22 );
			}
		}
	}
}

Tour_Settings_Tab_Phys::init();



