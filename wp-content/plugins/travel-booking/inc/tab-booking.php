<?php
// add tab
add_action( 'woocommerce_product_write_panel_tabs', 'tour_booking_options_tab_phys' );
function tour_booking_options_tab_phys() {
	echo '<li class="show_if_tour_phys"><a href="#tour_booking_phys">' . __( 'Tour Booking', 'travel-booking' ) . '</a></li>';
	echo '<li class="show_if_tour_phys"><a href="#tour_data_phys">' . __( 'Tour Data', 'travel-booking' ) . '</a></li>';
}

// add field
if ( TravelBookingPhyscode::$_version_woo < 3 ) {
	add_action( 'woocommerce_product_write_panels', 'tour_booking_options_physcode' );
} elseif ( TravelBookingPhyscode::$_version_woo == 3 ) {
	add_action( 'woocommerce_product_data_panels', 'tour_booking_options_physcode' );
} else {
	add_action( 'woocommerce_product_data_panels', 'tour_booking_options_physcode' );
}

function tour_booking_options_physcode() {
	include_once( 'metabox-booking.php' );
}

// Save field
add_action( 'woocommerce_process_product_meta', 'save_all_field_tour_custom_phys' );
function save_all_field_tour_custom_phys( $post_id ) {
	$tour_code          = $_POST['tour_code'];
	$tour_number_ticket = $_POST['tour_number_ticket'];
	$price_child        = $_POST['price_child'];
	$tour_duration      = $_POST['tour_duration'];
	$tour_start_date    = $_POST['tour_start_date'];
	$tour_end_date      = $_POST['tour_end_date'];
	$price_each_day     = $_POST['price_each_day'];
	$date_finish_tour   = $_POST['date_finish_tour'];
	$tour_itinerary     = $_POST['tour_content_itinerary'];
	$tour_address       = $_POST['tour_address'];
	$tour_lat           = $_POST['tour_lat'];
	$tour_long          = $_POST['tour_long'];

	update_post_meta( $post_id, '_tour_code', $tour_code );
	update_post_meta( $post_id, '_tour_number_ticket', $tour_number_ticket );
	update_post_meta( $post_id, '_price_child', $price_child );
	update_post_meta( $post_id, '_tour_duration', $tour_duration );
	update_post_meta( $post_id, '_tour_start_date', $tour_start_date );
	update_post_meta( $post_id, '_tour_end_date', $tour_end_date );
	update_post_meta( $post_id, '_price_each_day', $price_each_day );
	update_post_meta( $post_id, '_date_finish_tour', $date_finish_tour );
	update_post_meta( $post_id, '_tour_content_itinerary', $tour_itinerary );
	update_post_meta( $post_id, '_tour_location_address', $tour_address );
	update_post_meta( $post_id, '_tour_location_lat', $tour_lat );
	update_post_meta( $post_id, '_tour_location_long', $tour_long );
}