<?php
/**
 * Rewrite url tours
 *
 * @author    physcode
 */

add_filter( 'post_type_link', 'filter_post_type_link_phys', 11, 2 );
function filter_post_type_link_phys( $post_link, $post ) {
	if ( 'product' == $post->post_type ) {
		$product = @wc_get_product( $post );
		if ( $product->is_type( 'tour_phys' ) ) {
			static $product_struct, $is_complex_struct, $cache = array();

			$cache_key = $post_link;
			if ( isset( $cache[$cache_key] ) ) {
				return $cache[$cache_key];
			}

			if ( null === $product_struct ) {
				if ( isset( $GLOBALS['wp_rewrite']->extra_permastructs['product'] ) ) {
					$product_struct_init = $GLOBALS['wp_rewrite']->extra_permastructs['product']['struct'];
					$mapper              = array(
						'`%product%`' => '{XPRODUCTX}',
						'`%\w+%`'     => '[^/]+',
					);
					$product_struct      = preg_replace( array_keys( $mapper ), $mapper, ( $product_struct_init && $product_struct_init[0] != '/' ? '/' : '' ) . $product_struct_init );
					$is_complex_struct   = strpos( $product_struct, '[^/]+' ) !== false;
				}
			}

			$what_replace   = str_replace( '{XPRODUCTX}', $post->post_name, $product_struct );
			$full_tour_slug = get_tour_base_rewrite_rule_phys( true ) . $post->post_name;
			if ( $is_complex_struct ) {
				$fixed_link = preg_replace( '`' . $what_replace . '`', $full_tour_slug, $post_link );
			} else {
				$fixed_link = str_replace( $what_replace, $full_tour_slug, $post_link );
			}
			$cache[$cache_key] = $fixed_link;

			return $fixed_link;
		}
	}

	return $post_link;
}

add_filter( 'rewrite_rules_array', 'filter_rewrite_rules_array_phys' );
function filter_rewrite_rules_array_phys( $rules ) {
	$tour_base_url = get_tour_base_rewrite_rule_phys( true, true );
	if ( $tour_base_url ) {
		$tour_base_url = ltrim( $tour_base_url, '/' );
		$new_rules     = array(
			$tour_base_url . 'page/([0-9]{1,})/?' => 'index.php?is_tour=1&paged=$matches[1]',
			$tour_base_url . '?$'                 => 'index.php?is_tour=1', // &post_type=product&product_type=tour_phys

			$tour_base_url . '([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$' => 'index.php?product=$matches[1]&feed=$matches[2]',
			$tour_base_url . '([^/]+)/(feed|rdf|rss|rss2|atom)/?$'      => 'index.php?product=$matches[1]&feed=$matches[2]',
			$tour_base_url . '([^/]+)/comment-page-([0-9]{1,})/?'       => 'index.php?product=$matches[1]&cpage=$matches[2]',
			$tour_base_url . '(.+)'                                     => 'index.php?product=$matches[1]',
		);

		return array_merge( $new_rules, $rules );
	}

	return $rules;
}

function get_tour_base_rewrite_rule_phys( $with_front = false, $reset_cache = false ) {
	static $base_url, $base_full, $is_front_page, $default_lang, $cur_lang;

	if ( null === $base_url || $reset_cache ) {
		$tours_page_id = get_option( Tour_Settings_Tab_Phys::$_tours_show_page_id );

		if ( $tours_page_id && 'page' == get_option( 'show_on_front' ) && $tours_page_id == get_option( 'page_on_front' ) ) {
			$base_url      = '';
			$is_front_page = true;
		} else {
			$base_url = $tours_page_id && $tours_page_id != '' ? get_page_uri( $tours_page_id ) : 'tours';
			if ( $base_url ) {
				$base_url .= '/';
			}
		}
	}

	if ( $with_front ) {
		if ( null === $base_full || $reset_cache ) {
			$front = $GLOBALS['wp_rewrite']->front;
			if ( $front != '/index.php/' ) {
				$front = '/';
			}
			$base_full = $base_url || $is_front_page ? $front . $base_url : '';
		}

		return $base_full;
	}

	return $base_url;
}

add_action( 'init', 'rewrite_slug_attribute_woo_by_phys' );
function rewrite_slug_attribute_woo_by_phys() {
	$taxonomies    = get_object_taxonomies( 'product', 'objects' );
	$attribute_arr = array();

	if ( empty( $taxonomies ) ) {
		return '';
	}

	foreach ( $taxonomies as $tax ) {
		$tax_name = $tax->name;
		if ( 0 !== strpos( $tax_name, 'pa_' ) ) {
			continue;
		}
		if ( ! in_array( $tax_name, $attribute_arr ) ) {
			$attribute_arr[$tax_name] = $tax_name;
		}
	}

	foreach ( $attribute_arr as $k => $attr ) {
		$taxonomy_attr = get_taxonomy($attr);
		$attr_name    = str_replace( 'pa_', '', $attr );
		$slug_rewrite = 'tour-' . $attr_name;
		if ( $slug_rewrite == 'tour-destination' ) {
			$slug_rewrite = apply_filters( 'slug-tour-destination', $slug_rewrite );
		} elseif ( $slug_rewrite == 'tour-month' ) {
			$slug_rewrite = apply_filters( 'slug-tour-month', $slug_rewrite );
		}

		$label                    = $attr_name;
		$taxonomy_data            = array();
		$taxonomy_data            = array(
			'labels'       => array(
				'name'              => $label,
				'singular_name'     => $taxonomy_attr->labels->singular_name,
				'search_items'      => sprintf( __( 'Search %s', 'woocommerce' ), $label ),
				'all_items'         => sprintf( __( 'All %s', 'woocommerce' ), $label ),
				'parent_item'       => sprintf( __( 'Parent %s', 'woocommerce' ), $label ),
				'parent_item_colon' => sprintf( __( 'Parent %s:', 'woocommerce' ), $label ),
				'edit_item'         => sprintf( __( 'Edit %s', 'woocommerce' ), $label ),
				'update_item'       => sprintf( __( 'Update %s', 'woocommerce' ), $label ),
				'add_new_item'      => sprintf( __( 'Add New %s', 'woocommerce' ), $label ),
				'new_item_name'     => sprintf( __( 'New %s', 'woocommerce' ), $label ),
				'not_found'         => sprintf( __( 'No &quot;%s&quot; found', 'woocommerce' ), $label ),
			),
			'show_in_menu' => false,
		);
		$taxonomy_data['rewrite'] = array(
			'slug'         => '/' . $slug_rewrite,
			'with_front'   => false,
			'hierarchical' => true
		);
		register_taxonomy( $attr, apply_filters( "woocommerce_taxonomy_objects_{$attr}", array( 'product' ) ), apply_filters( "woocommerce_taxonomy_args_{$attr}", $taxonomy_data ) );

		flush_rewrite_rules();
		delete_transient( 'wc_attribute_taxonomies' );
	}
}

// Menu tour frontend
add_filter( 'wp_nav_menu_objects', 'filter_nav_menu_item_tour_phys', 11, 1 );
function filter_nav_menu_item_tour_phys( $menu_items ) {
	if ( ! is_woocommerce() ) {
		return $menu_items;
	}

	$hotel_page = get_option( Tour_Settings_Tab_Phys::$_tours_show_page_id );
	$shop_page  = (int) wc_get_page_id( 'shop' );

	foreach ( (array) $menu_items as $key => $menu_item ) {

		$classes = (array) $menu_item->classes;

		if ( get_query_var( 'is_tour' ) && $hotel_page == $menu_item->object_id && 'page' === $menu_item->object ) {
			$menu_items[$key]->current = true;
			$classes[]                 = 'current-menu-item';
			$classes[]                 = 'current_page_item';

		} elseif ( is_singular( 'product' ) && !get_query_var('is-single-tour') ) {
			if($shop_page == $menu_item->object_id) {
				$classes[] = 'current_page_parent';
			} else {
				unset( $classes[array_search( 'current_page_parent', $classes )] );
			}
		} elseif ( get_query_var( 'is_tour' ) && is_shop() && $shop_page == $menu_item->object_id && 'page' === $menu_item->object ) {
			$menu_items[$key]->current = false;
			unset( $classes[array_search( 'current-menu-item', $classes )] );
			unset( $classes[array_search( 'current_page_item', $classes )] );
		} elseif (get_query_var('is-single-tour')) {
			if($hotel_page == $menu_item->object_id) {
				$classes[] = 'current_page_parent';
			} else {
				unset( $classes[array_search( 'current_page_parent', $classes )] );
			}
		}

		$menu_items[$key]->classes = array_unique( $classes );

	}

	//		echo '<pre>' . print_r( $menu_items, true ) . '</pre>';

	return $menu_items;
}