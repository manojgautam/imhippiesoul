<?php
/*** Ajax url ***/
add_action( 'wp_print_scripts', 'tour_booking_phys_ajax_url' );
function tour_booking_phys_ajax_url() {
	echo '<script type="text/javascript">
		var tb_phys_ajax_url ="' . get_site_url() . '/";
		</script>';
}

/*** Add Tour to cart ***/
//add_action('wp_ajax_add_tour_to_cart_phys', 'add_tour_to_cart_phys');
//add_action('wp_ajax_nopriv_add_tour_to_cart_phys', 'add_tour_to_cart_phys');
add_action( 'tb_ajax_add_tour_to_cart_phys', 'add_tour_to_cart_phys' );
function add_tour_to_cart_phys() {
	$message = array( 'status' => 'error' );
	//	if ( isset( $_REQUEST['nonce'] ) && wp_verify_nonce( $_REQUEST['nonce'], 'tb_booking_nonce_action' ) ) {
	$date_now       = date( 'Y-m-d' );
	$show_date_book = apply_filters( 'tb_show_date_book', true );

	if ( ! isset( $_POST['tour_id'] ) || ( $show_date_book && ! isset( $_POST['date_booking'] ) ) ) {
		wp_send_json( $message );
	}

	if ( ! $show_date_book ) {
		$_POST['date_booking'] = $date_now;
	}

	do_action( 'woocommerce_set_cart_cookies', true );

	$tour_id         = $_POST['tour_id'];
	$date_booking    = $_POST['date_booking'];
	$number_ticket   = $_POST['number_ticket'];
	$cart_tour       = WC()->cart;
	$now             = strtotime( date( 'Y-m-d H:i:s' ) );
	$basket_item_key = $cart_tour->generate_cart_id( $now );
	$tour            = wc_get_product( $tour_id );

	$cart_tour->cart_contents[$basket_item_key] = array(
		'product_id'   => $tour_id,
		'variation_id' => 0,
		'variation'    => 0,
		'quantity'     => $number_ticket,
		'date_booking' => $date_booking,
		'data'         => $tour,
	);

	if ( isset( $_POST['number_children'] ) ) {
		$cart_tour->cart_contents[$basket_item_key]['number_children'] = $_POST['number_children'];
		$price_children                                                = get_price_for_child_on_tour( $tour, $date_booking );
		$cart_tour->cart_contents[$basket_item_key]['price_children']  = $price_children;
	}

	$cart_tour->calculate_totals();

	$message['status'] = 'success';
	//	}
	wp_send_json( $message );
}

add_action( 'woocommerce_before_calculate_totals', 'set_price_day_phys' );
function set_price_day_phys( $cart_object ) {
	foreach ( $cart_object->cart_contents as $key => $value ) {
		if ( isset( $value['date_booking'] ) ) {
			$timestamp   = strtotime( $value['date_booking'] );
			$day_booking = date( "l", $timestamp );
			$tour_days   = json_decode( htmlspecialchars_decode( get_post_meta( $value['product_id'], '_price_each_day', true ) ) );
			if ( isset( $tour_days->{$day_booking} ) ) {
				if ( isset( $tour_days->{$day_booking}->price ) ) {
					$price_day = $tour_days->{$day_booking}->price;
				} else {
					$price_day = $tour_days->{$day_booking};
				}
				if ( $price_day != 0 ) {
					if ( TravelBookingPhyscode::$_version_woo < 3 ) {
						$value['data']->price = $price_day;
					} elseif ( TravelBookingPhyscode::$_version_woo >= 3 ) {
						$value['data']->set_price( $price_day );
					}
				}
			}
		}
	}
}

add_action( 'woocommerce_after_calculate_totals', 'update_price_have_price_child' );
function update_price_have_price_child( $cart_object ) {
	if ( get_option( 'show_adults_children' ) ) {
		if ( TravelBookingPhyscode::$_version_woo < 3.2 ) {
			foreach ( $cart_object->cart_session_data as $key => $default ) {
				$cart_object->$key = $default;
			}
			do_action( 'woocommerce_cart_reset', $cart_object, false );

			$cart_object->coupons = $cart_object->get_coupons();

			if ( $cart_object->is_empty() ) {
				$cart_object->set_session();

				return;
			}

			$tax_rates      = array();
			$shop_tax_rates = array();
			$cart           = $cart_object->get_cart();

			/**
			 * Calculate subtotals for items. This is done first so that discount logic can use the values.
			 */
			foreach ( $cart as $cart_item_key => $values ) {
				$_product = $values['data'];

				/** Update number child if update cart **/
				if ( isset( $_POST['cart'] ) ) {
					if ( isset( $values['number_children'] ) ) {
						$cart[$cart_item_key]['number_children']                       = $_POST['cart'][$cart_item_key]['number_children'];
						$cart_object->cart_contents[$cart_item_key]['number_children'] = $_POST['cart'][$cart_item_key]['number_children'];
					}
				}

				if ( isset( $values['number_children'] ) ) {
					$line_price_child = get_price_for_child_on_tour( $_product, $values['date_booking'] );
					$line_price       = $_product->get_price() * $values['quantity'] + $line_price_child * $values['number_children'];

					// set for order info
					$cart_object->cart_contents[$cart_item_key]['price_adults']   = $_product->get_price();
					$cart_object->cart_contents[$cart_item_key]['price_children'] = $line_price_child;
					$cart_object->cart_contents[$cart_item_key]['currency']       = get_woocommerce_currency_symbol( get_woocommerce_currency() );
				} else {
					$line_price = $_product->get_price() * $values['quantity'];
				}
				$line_subtotal     = 0;
				$line_subtotal_tax = 0;

				/**
				 * No tax to calculate.
				 */
				if ( ! $_product->is_taxable() ) {

					// Subtotal is the undiscounted price
					$cart_object->subtotal        += $line_price;
					$cart_object->subtotal_ex_tax += $line_price;

					/**
					 * Prices include tax.
					 *
					 * To prevent rounding issues we need to work with the inclusive price where possible.
					 * otherwise we'll see errors such as when working with a 9.99 inc price, 20% VAT which would.
					 * be 8.325 leading to totals being 1p off.
					 *
					 * Pre tax coupons come off the price the customer thinks they are paying - tax is calculated.
					 * afterwards.
					 *
					 * e.g. $100 bike with $10 coupon = customer pays $90 and tax worked backwards from that.
					 */
				} elseif ( $cart_object->prices_include_tax ) {

					// Get base tax rates
					if ( empty( $shop_tax_rates[$_product->tax_class] ) ) {
						$shop_tax_rates[$_product->tax_class] = WC_Tax::get_base_tax_rates( $_product->tax_class );
					}

					// Get item tax rates
					if ( empty( $tax_rates[$_product->get_tax_class()] ) ) {
						$tax_rates[$_product->get_tax_class()] = WC_Tax::get_rates( $_product->get_tax_class() );
					}

					$base_tax_rates = $shop_tax_rates[$_product->tax_class];
					$item_tax_rates = $tax_rates[$_product->get_tax_class()];

					/**
					 * ADJUST TAX - Calculations when base tax is not equal to the item tax.
					 *
					 * The woocommerce_adjust_non_base_location_prices filter can stop base taxes being taken off when dealing with out of base locations.
					 * e.g. If a product costs 10 including tax, all users will pay 10 regardless of location and taxes.
					 * This feature is experimental @since 2.4.7 and may change in the future. Use at your risk.
					 */
					if ( $item_tax_rates !== $base_tax_rates && apply_filters( 'woocommerce_adjust_non_base_location_prices', true ) ) {

						// Work out a new base price without the shop's base tax
						$taxes = WC_Tax::calc_tax( $line_price, $base_tax_rates, true, true );

						// Now we have a new item price (excluding TAX)
						$line_subtotal = $line_price - array_sum( $taxes );

						// Now add modified taxes
						$tax_result        = WC_Tax::calc_tax( $line_subtotal, $item_tax_rates );
						$line_subtotal_tax = array_sum( $tax_result );

						/**
						 * Regular tax calculation (customer inside base and the tax class is unmodified.
						 */
					} else {

						// Calc tax normally
						$taxes             = WC_Tax::calc_tax( $line_price, $item_tax_rates, true );
						$line_subtotal_tax = array_sum( $taxes );
						$line_subtotal     = $line_price - array_sum( $taxes );
					}

					/**
					 * Prices exclude tax.
					 *
					 * This calculation is simpler - work with the base, untaxed price.
					 */
				} else {

					// Get item tax rates
					if ( empty( $tax_rates[$_product->get_tax_class()] ) ) {
						$tax_rates[$_product->get_tax_class()] = WC_Tax::get_rates( $_product->get_tax_class() );
					}

					$item_tax_rates = $tax_rates[$_product->get_tax_class()];

					// Base tax for line before discount - we will store this in the order data
					$taxes             = WC_Tax::calc_tax( $line_price, $item_tax_rates );
					$line_subtotal_tax = array_sum( $taxes );

					$line_subtotal = $line_price;
				}

				// Add to main subtotal
				$cart_object->subtotal += $line_subtotal + $line_subtotal_tax;;
				$cart_object->subtotal_ex_tax += $line_subtotal;
			}

			// Order cart items by price so coupon logic is 'fair' for customers and not based on order added to cart.
			//	uasort( $cart, array( $cart_object, 'sort_by_subtotal' ) );

			/**
			 * Calculate totals for items.
			 */
			foreach ( $cart as $cart_item_key => $values ) {

				$_product = $values['data'];
				// Prices
				if ( isset( $values['number_children'] ) ) {
					$line_price_child = get_price_for_child_on_tour( $_product, $values['date_booking'] );

					$base_price = $_product->get_price() * $values['quantity'] + $line_price_child * $values['number_children'];
					$line_price = $_product->get_price() * $values['quantity'] + $line_price_child * $values['number_children'];
				} else {
					$base_price = $_product->get_price() * $values['quantity'];
					$line_price = $_product->get_price() * $values['quantity'];
				}

				// Tax data
				$taxes            = array();
				$discounted_taxes = array();

				/**
				 * No tax to calculate.
				 */
				if ( ! $_product->is_taxable() ) {

					// Discounted Price (price with any pre-tax discounts applied)
					$discounted_price  = $cart_object->get_discounted_price( $values, $base_price, true );
					$line_subtotal_tax = 0;
					$line_subtotal     = $line_price;
					$line_tax          = 0;
					$line_total        = round( $discounted_price, wc_get_rounding_precision() );

					/**
					 * Prices include tax.
					 */
				} elseif ( $cart_object->prices_include_tax ) {
					$base_tax_rates = $shop_tax_rates[$_product->tax_class];
					$item_tax_rates = $tax_rates[$_product->get_tax_class()];

					/**
					 * ADJUST TAX - Calculations when base tax is not equal to the item tax.
					 *
					 * The woocommerce_adjust_non_base_location_prices filter can stop base taxes being taken off when dealing with out of base locations.
					 * e.g. If a product costs 10 including tax, all users will pay 10 regardless of location and taxes.
					 * This feature is experimental @since 2.4.7 and may change in the future. Use at your risk.
					 */
					if ( $item_tax_rates !== $base_tax_rates && apply_filters( 'woocommerce_adjust_non_base_location_prices', true ) ) {

						// Work out a new base price without the shop's base tax
						$taxes = WC_Tax::calc_tax( $line_price, $base_tax_rates, true, true );

						// Now we have a new item price (excluding TAX)
						$line_subtotal     = round( $line_price - array_sum( $taxes ), wc_get_rounding_precision() );
						$taxes             = WC_Tax::calc_tax( $line_subtotal, $item_tax_rates );
						$line_subtotal_tax = array_sum( $taxes );

						// Adjusted price (this is the price including the new tax rate)
						$adjusted_price = ( $line_subtotal + $line_subtotal_tax ) / $values['quantity'];

						// Apply discounts and get the discounted price FOR A SINGLE ITEM
						$discounted_price = $cart_object->get_discounted_price( $values, $adjusted_price, true );

						// Convert back to line price and round nicely
						$discounted_line_price = round( $discounted_price * $values['quantity'], $cart_object->dp );

						// Now use rounded line price to get taxes.
						$discounted_taxes = WC_Tax::calc_tax( $discounted_line_price, $item_tax_rates, true );
						$line_tax         = array_sum( $discounted_taxes );
						$line_total       = $discounted_line_price - $line_tax;

						/**
						 * Regular tax calculation (customer inside base and the tax class is unmodified.
						 */
					} else {

						// Work out a new base price without the item tax
						$taxes = WC_Tax::calc_tax( $line_price, $item_tax_rates, true );
						// Now we have a new item price (excluding TAX)
						$line_subtotal     = $line_price - array_sum( $taxes );
						$line_subtotal_tax = array_sum( $taxes );

						// Calc prices and tax (discounted)
						$discounted_price = $cart_object->get_discounted_price( $values, $base_price, true );

						// Convert back to line price and round nicely
						$discounted_line_price = round( $discounted_price, $cart_object->dp );

						// Now use rounded line price to get taxes.
						$discounted_taxes = WC_Tax::calc_tax( $discounted_line_price, $item_tax_rates, true );
						$line_tax         = array_sum( $discounted_taxes );
						$line_total       = $discounted_line_price - $line_tax;
					}

					// Tax rows - merge the totals we just got
					foreach ( array_keys( $cart_object->taxes + $discounted_taxes ) as $key ) {
						$cart_object->taxes[$key] = ( isset( $discounted_taxes[$key] ) ? $discounted_taxes[$key] : 0 ) + ( isset( $cart_object->taxes[$key] ) ? $cart_object->taxes[$key] : 0 );
					}

					/**
					 * Prices exclude tax.
					 */
				} else {

					$item_tax_rates = $tax_rates[$_product->get_tax_class()];

					// Work out a new base price without the shop's base tax
					$taxes = WC_Tax::calc_tax( $line_price, $item_tax_rates );

					// Now we have the item price (excluding TAX)
					$line_subtotal     = $line_price;
					$line_subtotal_tax = array_sum( $taxes );

					// Now calc product rates
					$discounted_price      = $cart_object->get_discounted_price( $values, $base_price, true );
					$discounted_taxes      = WC_Tax::calc_tax( $discounted_price, $item_tax_rates );
					$discounted_tax_amount = array_sum( $discounted_taxes );
					$line_tax              = $discounted_tax_amount;
					$line_total            = $discounted_price;

					// Tax rows - merge the totals we just got
					foreach ( array_keys( $cart_object->taxes + $discounted_taxes ) as $key ) {
						$cart_object->taxes[$key] = ( isset( $discounted_taxes[$key] ) ? $discounted_taxes[$key] : 0 ) + ( isset( $cart_object->taxes[$key] ) ? $cart_object->taxes[$key] : 0 );
					}
				}

				// Cart contents total is based on discounted prices and is used for the final total calculation
				$cart_object->cart_contents_total += $line_total;

				// Store costs + taxes for lines
				$cart_object->cart_contents[$cart_item_key]['line_total']        = $line_total;
				$cart_object->cart_contents[$cart_item_key]['line_tax']          = $line_tax;
				$cart_object->cart_contents[$cart_item_key]['line_subtotal']     = $line_subtotal;
				$cart_object->cart_contents[$cart_item_key]['line_subtotal_tax'] = $line_subtotal_tax;

				// Store rates ID and costs - Since 2.2
				$cart_object->cart_contents[$cart_item_key]['line_tax_data'] = array(
					'total'    => $discounted_taxes,
					'subtotal' => $taxes
				);
			}

			// Only calculate the grand total + shipping if on the cart/checkout
			if ( is_checkout() || is_cart() || defined( 'WOOCOMMERCE_CHECKOUT' ) || defined( 'WOOCOMMERCE_CART' ) ) {

				// Calculate the Shipping
				$cart_object->calculate_shipping();

				// Trigger the fees API where developers can add fees to the cart
				$cart_object->calculate_fees();

				// Total up/round taxes and shipping taxes
				if ( $cart_object->round_at_subtotal ) {
					$cart_object->tax_total          = WC_Tax::get_tax_total( $cart_object->taxes );
					$cart_object->shipping_tax_total = WC_Tax::get_tax_total( $cart_object->shipping_taxes );
					$cart_object->taxes              = array_map( array( 'WC_Tax', 'round' ), $cart_object->taxes );
					$cart_object->shipping_taxes     = array_map(
						array(
							'WC_Tax',
							'round'
						), $cart_object->shipping_taxes
					);
				} else {
					$cart_object->tax_total          = array_sum( $cart_object->taxes );
					$cart_object->shipping_tax_total = array_sum( $cart_object->shipping_taxes );
				}

				// VAT exemption done at this point - so all totals are correct before exemption
				if ( WC()->customer->is_vat_exempt() ) {
					$cart_object->remove_taxes();
				}

				// Allow plugins to hook and alter totals before final total is calculated
				//			do_action( 'woocommerce_calculate_totals', $cart_object );

				// Grand Total - Discounted product prices, discounted tax, shipping cost + tax
				$cart_object->total = max( 0, apply_filters( 'woocommerce_calculated_total', round( $cart_object->cart_contents_total + $cart_object->tax_total + $cart_object->shipping_tax_total + $cart_object->shipping_total + $cart_object->fee_total, $cart_object->dp ), $cart_object ) );
			} else {

				// Set tax total to sum of all tax rows
				$cart_object->tax_total = WC_Tax::get_tax_total( $cart_object->taxes );

				// VAT exemption done at this point - so all totals are correct before exemption
				if ( WC()->customer->is_vat_exempt() ) {
					$cart_object->remove_taxes();
				}
			}
		} else {
			new Phys_Cart_Totals( $cart_object );
		}
	}
}

add_filter( 'woocommerce_cart_item_subtotal', 'update_subtotal_have_price_child', 10, 2 );
function update_subtotal_have_price_child( $value, $cart_item ) {
	$value = get_woocommerce_currency_symbol( get_woocommerce_currency() ) . $cart_item['line_subtotal'];

	return $value;
}

add_filter( 'woocommerce_add_order_item_meta', 'add_imgs_to_order', 10, 2 );
function add_imgs_to_order( $item_id, $tour_item ) {
	wc_add_order_item_meta( $item_id, '_date_booking', $tour_item['date_booking'] );
	if ( isset( $tour_item['number_children'] ) ) {
		wc_add_order_item_meta( $item_id, '_number_children', $tour_item['number_children'] );
	}
	if ( isset( $tour_item['price_adults'] ) ) {
		wc_add_order_item_meta( $item_id, '_price_adults', $tour_item['price_adults'] );
	}
	if ( isset( $tour_item['price_children'] ) ) {
		wc_add_order_item_meta( $item_id, '_price_children', $tour_item['price_children'] );
	}
	if ( isset( $tour_item['currency'] ) ) {
		wc_add_order_item_meta( $item_id, '_currency', $tour_item['currency'] );
	}
}

function get_price_for_child_on_tour( $tour, $date_booking ) {
	$price_children   = 0;
	$price_adults_day = 0;
	$timestamp        = strtotime( $date_booking );
	$day_booking      = date( "l", $timestamp );
	$tour_days        = json_decode( htmlspecialchars_decode( get_post_meta( $tour->get_id(), '_price_each_day', true ) ) );
	if ( isset( $tour_days->{$day_booking}->price_children ) ) {
		$price_children   = $tour_days->{$day_booking}->price_children;
		$price_adults_day = $tour_days->{$day_booking}->price;
	}
	if ( $price_children == 0 ) {
		$price_children = get_post_meta( $tour->get_id(), '_price_child', true ) ? get_post_meta( $tour->get_id(), '_price_child', true ) : 0;
	}
	if ( $price_children == 0 ) {
		$price_children = get_option( 'price_children' ) * $tour->get_price() / 100;
	}

	return $price_children;
}

/*** Get notify list tour or product of new order ***/
add_action( 'tb_ajax_notify_new_order', 'notify_new_order' );
function notify_new_order() {
	$message = array( 'status' => 'error' );

	if ( isset( $_POST['limit'] ) ) {
		$limit = $_POST['limit'];
	} else {
		$message['status'] = 'error';
		wp_send_json( $message );
	}

	$args = array(
		'status'      => 'wc-completed',
		'type'        => wc_get_order_types( 'view-orders' ),
		'parent'      => null,
		'customer'    => null,
		'email'       => '',
		'limit'       => $limit,
		'offset'      => null,
		'exclude'     => array(),
		'orderby'     => 'date',
		'order'       => 'DESC',
		'return'      => 'objects',
		'paginate'    => false,
		'date_before' => '',
		'date_after'  => '',
	);

	$orders_tour = wc_get_orders( $args );

	$html = '';
	if ( count( $orders_tour ) > 0 ) {
		foreach ( $orders_tour as $order_tour ) {
			$items    = $order_tour->get_items();
			$key_item = array_keys( $items );
			$rand     = 0;
			if ( count( $key_item ) > 1 ) {
				$rand = rand( 0, count( $key_item ) - 1 );
			}
			$product = $items[$key_item[$rand]];
			$html    .= '<div class="item-order"><div class="inner-content"><a href="' . get_permalink( $product->get_product_id() ) . '">';
			$html    .= '<img src="' . get_the_post_thumbnail_url( $product->get_product_id(), 'thumbnail' ) . '" /></a>';
			$html    .= '<span>' . esc_html__( 'Someone in ', 'travel-booking' ) . $order_tour->get_billing_address_1() . ', ' . $order_tour->get_billing_city() . ', ' . $order_tour->get_billing_country() . esc_html__( ' purchased a ', 'travel-booking' ) . '</span>';
			$html    .= '<a href="' . get_permalink( $product->get_product_id() ) . '" class="title">' . $product->get_name() . '</a>';
			$html    .= '<span class="date">' . caculate_about_time( $order_tour->get_date_created() ) . '</span>';
			$html    .= '</div> </div>';
		}

		$message['status'] = 'success';
		$message['html']   = $html;
	}

	wp_send_json( $message );
}
