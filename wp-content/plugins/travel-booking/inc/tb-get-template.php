<?php
/***
 *
 */
if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

function tb_template_path() {
	$tb_template_path = get_stylesheet_directory() . DIRECTORY_SEPARATOR . 'travel-booking' . DIRECTORY_SEPARATOR;

	return apply_filters( 'tb_template_path', $tb_template_path );
}

function tb_get_file_template( $file_name, $include_one = true, $not_include = false ) {
	$tb_template_parent_path = get_template_directory() . DIRECTORY_SEPARATOR . 'travel-booking' . DIRECTORY_SEPARATOR;
	$check_file              = tb_template_path() . $file_name;
	$check_file_of_parent    = $tb_template_parent_path . $file_name;
	if ( $not_include ) {
		if ( file_exists( $check_file ) ) {
			return $check_file;
		} elseif ( file_exists( $check_file_of_parent ) ) {
			return $check_file_of_parent;
		} else {
			return TB_PHYS_TEMPLATE_PATH_DEFAULT . $file_name;
		}

		return TB_PHYS_TEMPLATE_PATH_DEFAULT . $file_name;
	} elseif ( file_exists( $check_file ) ) {
		if ( $include_one ) {
			include_once( $check_file );
		} else {
			include( $check_file );
		}
	} elseif ( file_exists( $check_file_of_parent ) ) {
		if ( $include_one ) {
			include_once( $check_file_of_parent );
		} else {
			include( $check_file_of_parent );
		}
	} else {
		if ( $include_one ) {
			include_once( TB_PHYS_TEMPLATE_PATH_DEFAULT . $file_name );
		} else {
			include( TB_PHYS_TEMPLATE_PATH_DEFAULT . $file_name );
		}
	}
}

if ( !function_exists( 'tour_booking_single_title' ) ) {
	function tour_booking_single_title() {
		tb_get_file_template( 'single-tour' . DIRECTORY_SEPARATOR . 'title.php' );
	}
}

if ( !function_exists( 'tour_booking_breadcrumb' ) ) {
	function tour_booking_breadcrumb() {
		tb_get_file_template( 'global' . DIRECTORY_SEPARATOR . 'breadcrumb.php' );
	}
}

if ( !function_exists( 'tour_booking_single_ratting' ) ) {
	function tour_booking_single_ratting() {
		tb_get_file_template( 'single-tour' . DIRECTORY_SEPARATOR . 'review-rating.php' );
	}
}

if ( !function_exists( 'tour_booking_single_code' ) ) {
	function tour_booking_single_code() {
		tb_get_file_template( 'single-tour' . DIRECTORY_SEPARATOR . 'code.php' );
	}
}

if ( !function_exists( 'tour_booking_single_price' ) ) {
	function tour_booking_single_price() {
		tb_get_file_template( 'single-tour' . DIRECTORY_SEPARATOR . 'price.php' );
	}
}

if ( !function_exists( 'tour_booking_single_booking' ) ) {
	function tour_booking_single_booking() {
		tb_get_file_template( 'single-tour' . DIRECTORY_SEPARATOR . 'booking.php' );
	}
}

if ( !function_exists( 'tour_booking_single_gallery' ) ) {
	function tour_booking_single_gallery() {
		tb_get_file_template( 'single-tour' . DIRECTORY_SEPARATOR . 'gallery.php' );
	}
}

if ( !function_exists( 'tour_booking_default_product_tabs' ) ) {
	function tour_booking_default_product_tabs( $tabs = array() ) {
		global $product, $post;
		$tab_field_itinerary  = get_post_meta( $post->ID, '_tour_content_itinerary', true );
		$tab_location_address = get_post_meta( $post->ID, '_tour_location_address', true );
		// Description tab - shows product content

		if ( $post->post_content ) {
			$tabs['description'] = array(
				'title'    => __( 'Description', 'travel-booking' ),
				'priority' => 10,
				'callback' => 'woocommerce_product_description_tab'
			);
		}
		if ( $tab_field_itinerary ) {
			$tabs['itinerary_tab'] = array(
				'title'    => __( 'Itinerary', 'travel-booking' ),
				'priority' => 20,
				'callback' => 'tour_booking_itinerary_tab'
			);
		}

		if ( $tab_location_address ) {
			$tabs['location_tab'] = array(
				'title'    => __( 'Location', 'travel-booking' ),
				'priority' => 21,
				'callback' => 'tour_booking_location_tab'
			);
		}

		// Reviews tab - shows comments
		if ( comments_open() ) {
			$tabs['reviews'] = array(
				'title'    => sprintf( __( 'Reviews (%d)', 'travel-booking' ), $product->get_review_count() ),
				'priority' => 30,
				'callback' => 'comments_template'
			);
		}

		return $tabs;
	}
}


if ( ! function_exists( 'tour_booking_sort_product_tabs' ) ) {

	/**
	 * Sort tabs by priority.
	 *
	 * @param array $tabs
	 * @return array
	 */
	function tour_booking_sort_product_tabs( $tabs = array() ) {

		// Make sure the $tabs parameter is an array
		if ( ! is_array( $tabs ) ) {
			trigger_error( "Function woocommerce_sort_product_tabs() expects an array as the first parameter. Defaulting to empty array." );
			$tabs = array();
		}

		// Re-order tabs by priority
		if ( ! function_exists( '_sort_priority_callback' ) ) {
			function _sort_priority_callback( $a, $b ) {
				if ( $a['priority'] === $b['priority'] )
					return 0;
				return ( $a['priority'] < $b['priority'] ) ? -1 : 1;
			}
		}

		uasort( $tabs, '_sort_priority_callback' );

		return $tabs;
	}
}

if ( !function_exists( 'tour_booking_location_tab' ) ) {
	function tour_booking_location_tab() {
		global $post;
		$tab_location_address = get_post_meta( $post->ID, '_tour_location_address', true );
		$tab_location_lat     = get_post_meta( $post->ID, '_tour_location_lat', true );
		$tab_location_long    = get_post_meta( $post->ID, '_tour_location_long', true );
		//$api_key              = get_post_meta( $post->ID, '_tour_location_api_key', true );
		$api_key = get_option( 'google_api_key' );
		$data    = ' data-lat="' . esc_attr( $tab_location_lat ) . '"';
		$data .= ' data-long="' . esc_attr( $tab_location_long ) . '"';
		$data .= ' data-address="' . esc_attr( $tab_location_address ) . '"';
		echo '<div class="wrapper-gmap"><div id="googleMapCanvas" class="google-map"' . $data . '></div></div> ';
		wp_enqueue_script( 'google-map', "https://maps.googleapis.com/maps/api/js?key=" . $api_key . "", array( 'jquery' ), false, true );
		wp_enqueue_script( 'map', TOUR_BOOKING_PHYS_URL . 'assets/js/frontend/gmap.js', array( 'jquery' ), false, true );
	}
}

if ( !function_exists( 'tour_booking_itinerary_tab' ) ) {
	function tour_booking_itinerary_tab() {
		global $post;
		$tab_field_itinerary = get_post_meta( $post->ID, '_tour_content_itinerary', true );
		echo wpautop( $tab_field_itinerary );
	}
}

if ( !function_exists( 'tour_booking_single_information' ) ) {
	function tour_booking_single_information() {
		tb_get_file_template( 'single-tour' . DIRECTORY_SEPARATOR . 'infomation.php' );
	}
}

if ( !function_exists( 'tour_booking_single_related' ) ) {
	function tour_booking_single_related() {
		tb_get_file_template( 'single-tour' . DIRECTORY_SEPARATOR . 'related.php', false );
	}
}

