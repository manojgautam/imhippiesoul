<?php
/**
 *
 *  Widgets Tour
 *
 */

add_action( 'widgets_init', 'tb_load_widget_phys' );

function tb_load_widget_phys() {
	register_widget( 'tb_search_widget_phys' );
}

class tb_search_widget_phys extends WP_Widget {
	public function __construct() {
		$widget_ops = array(
			'classname'   => 'tb_search_widget',
			'description' => __( 'Tour Search', 'travel-booking' ),
		);
		parent::__construct( 'tb_search_widget_phys', 'Tour Search', $widget_ops );
	}
	public function form( $instance ) {
		$defaults       = array(
			'show_tour_code' => 'hide',
		);
		$instance       = wp_parse_args( (array) $instance, $defaults );
		$show_tour_code = $instance['show_tour_code'];
		?>
		<p>
			<label><?php echo esc_html__( 'Show Tour Code', 'travel-booking' ); ?></label>
			<select name="<?php echo ent2ncr( $this->get_field_name( 'show_tour_code' ) ); ?>" class="widefat">
				<option value="hide" <?php selected( $show_tour_code, 'hide' ) ?>><?php esc_html_e( 'Hide', 'travel-booking' ) ?></option>
				<option value="show" <?php selected( $show_tour_code, 'show' ) ?>><?php esc_html_e( 'Show', 'travel-booking' ) ?></option>
			</select>
		</p>
	<?php }

	public function widget( $args, $instance ) {
		extract( $args );
		$show_tour_code     = isset( $instance['show_tour_code'] ) ? $instance['show_tour_code'] : 'hide';

		$tour_tax_param = get_query_var( 'tourtax' );
		?>
		<div class="search_tour">
			<div class="form-block block-after-indent">
				<h3 class="form-block_title"><?php _e( 'Search Tour', 'travel-booking' ) ?></h3>
				<div class="form-block__description"><?php _e( 'Find your dream tour today!', 'travel-booking' ) ?></div>
				<form method="get" action="<?php echo home_url() ?>">
					<div class="form-field-input">
						<input type="hidden" name="tour_search" value="1">
						<input type="text" placeholder="<?php _e( 'Search Tour', 'travel-booking' ); ?>" value="" name="name_tour">
					</div>
					<?php
					if ( $show_tour_code == 'show' ) {
						?>
						<div class="form-field-input">
 							<input type="text" placeholder="<?php _e( 'Tour Code', 'travel-booking' ); ?>" value="" name="tour_code">
						</div>
					<?php } ?>
					<div class="form-field-select">
						<select name="tourtax[tour_phys]">
							<option value="0"><?php _e( 'Tour Type', 'travel-booking' ) ?></option>
							<?php
							$taxonomy   = 'tour_phys'; // taxonomy slug
							$tour_terms = get_terms( $taxonomy );
							if ( $tour_terms ) {
								foreach ( $tour_terms as $term ) {
									if ( $term->slug == $tour_tax_param[$taxonomy] ) {
										echo '<option value="' . $term->slug . '" selected="selected">' . $term->name . '</option>';
									} else {
										echo '<option value="' . $term->slug . '">' . $term->name . '</option>';
									}
								}
							}
							?>
						</select>
					</div>
					<?php
					if ( get_option( 'tour_search_by_attributes' ) ) {
						$option_attribute_to_search = get_option( 'tour_search_by_attributes' );
						foreach ( $option_attribute_to_search as $attribute_to_search ) {
							$tax_attribute      = get_taxonomy( $attribute_to_search );
							$terms_of_attribute = get_terms( $attribute_to_search );
							if ( count( $terms_of_attribute ) > 0 ) {
								echo '<div class="form-field-select"><select name="tourtax[' . $attribute_to_search . ']">';
								echo '<option value="0">' . $tax_attribute->labels->singular_name . '</option>';
								foreach ( $terms_of_attribute as $term ) {
									if ( $term->slug == $tour_tax_param[$attribute_to_search] ) {
										echo '<option value="' . $term->slug . '" selected="selected">' . $term->name . '</option>';
									} else {
										echo '<option value="' . $term->slug . '">' . $term->name . '</option>';
									}
								}
								echo '</select></div>';
							}
						}
					}
					?>
					<input type="hidden" name="lang" value="<?php echo isset( $_GET['lang'] ) ? $_GET['lang'] : '' ?>">
					<button type="submit"><?php _e( 'Find Tours', 'travel-booking' ) ?></button>
				</form>
			</div>
		</div>
		<?php
	}



	public function update( $new_instance, $old_instance ) {
		$instance                   = $old_instance;
		$instance['show_tour_code'] = sanitize_text_field( $new_instance['show_tour_code'] );

		return $instance;
	}
}