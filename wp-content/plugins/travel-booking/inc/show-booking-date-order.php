<?php
add_action( 'woocommerce_before_order_itemmeta', 'show_booking_date_order_phys', 10, 3 );
function show_booking_date_order_phys( $item_id, $item, $_product ) {
	$date_booking    = wc_get_order_item_meta( $item_id, '_date_booking', true );
	$qty             = wc_get_order_item_meta( $item_id, '_qty', true );
	$price_adults    = wc_get_order_item_meta( $item_id, '_price_adults', true );
	$number_children = wc_get_order_item_meta( $item_id, '_number_children', true );
	$price_children  = wc_get_order_item_meta( $item_id, '_price_children', true );
	$currency        = wc_get_order_item_meta( $item_id, '_currency', true );
	if ( $date_booking ) {
		echo '<p> ' . __( 'Booking Date', 'travel-booking' ) . ' : ' . $date_booking . '</p>';
		if ( isset( $price_adults ) ) {
			echo '<p> ' . __( 'Aduls', 'travel-booking' ) . ' ( ' . $currency . $price_adults . ' ) &times; ' . $qty . '</p>';
		}
		if ( isset( $number_children ) ) {
			echo '<p> ' . __( 'Children', 'travel-booking' ) . ' ( ' . $currency . $price_children . ' ) &times; ' . $number_children . '</p>';
		}
	}
}

add_filter( 'woocommerce_hidden_order_itemmeta', 'remove_order_item_meta_fields' );
function remove_order_item_meta_fields( $fields ) {
	$fields[] = '_date_booking';
	$fields[] = '_number_children';
	$fields[] = '_price_children';
	$fields[] = '_price_adults';
	$fields[] = '_currency';

	return $fields;
}