<?php
/**
 * Register product type tour
 */
add_action( 'plugins_loaded', 'register_product_type_tour_phys' );
function register_product_type_tour_phys() {
	class WC_Product_Tour_Phys extends WC_Product {
		public function __construct( $product ) {
			$this->product_type = 'tour_phys';
			parent::__construct( $product );
		}
	}
}

/**
 * show option Tour
 */
add_filter( 'product_type_selector', 'add_type_tour_to_product_phys' );
function add_type_tour_to_product_phys( $types ) {
	$types['tour_phys'] = __( 'Tour', 'travel-booking' );

	return $types;
}

/**
 * Show field price for Tour
 */
add_action( 'admin_footer', 'show_price_for_tour_phys' );
function show_price_for_tour_phys() {
	if ( 'product' != get_post_type() ) :
		return;
	endif;

	?>
	<script type='text/javascript'>
		jQuery(document).ready(function ($) {
			$('.options_group.pricing').addClass('show_if_tour_phys').show();
			if ($('#product-type').val() == 'tour_phys') {
				$('.general_tab').show();

				//show inventory on tab tour
//				$('.inventory_tab').show();
//				$('._manage_stock_field').show();
//				$('.advanced_options').hide();
//				$('._backorders_field').hide();
			}
		});
	</script>
	<?php
}

add_action( 'admin_menu', 'settings_menu', 2 );
function settings_menu() {
	global $submenu;
	if ( !empty( $submenu['edit.php?post_type=product'] ) ) {
		$productsMenu = &$submenu['edit.php?post_type=product'];
		array_unshift(
			$productsMenu,
			array(
				__( 'Tours', 'tours' ),
				'edit_products',
				'edit.php?post_type=product&product_type=tour_phys',
			) );
	}

    if (isset($_GET['product_type']) && $_GET['product_type'] == 'tour_phys') {
        echo '<input type="hidden" name="menu_tour" value="1">';
    }
}

add_action( 'manage_product_posts_custom_column', 'remove_action_show_product_admin', 5 );
function remove_action_show_product_admin( $column ) {
	global $post, $the_product;
	switch ( $column ) {
		case 'name' :
			if ( !isset( $_GET['product_type'] )) {
				if ( $the_product->get_type() == 'tour_phys' ) {
					echo '<input type="hidden" name="hidden_tour_phys" value="true">';
				}
			} else {
				echo '<input type="hidden" name="active_tour_css" value="true">';
			}

			break;
	}
}

