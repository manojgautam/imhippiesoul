<?php
/**
 * Metabox for Tour Booking
 */
global $post;
$tour_code          = get_post_meta( $post->ID, '_tour_code', true );
$tour_number_ticket = get_post_meta( $post->ID, '_tour_number_ticket', true );
$price_child        = get_post_meta( $post->ID, '_price_child', true ) ? get_post_meta( $post->ID, '_price_child', true ) : 0;
$tour_duration      = get_post_meta( $post->ID, '_tour_duration', true );
$tour_start_date    = get_post_meta( $post->ID, '_tour_start_date', true );
$tour_end_date      = get_post_meta( $post->ID, '_tour_end_date', true );
$date_finish_tour   = get_post_meta( $post->ID, '_date_finish_tour', true ) ? get_post_meta( $post->ID, '_date_finish_tour', true ) : 0;

// for each day
$a_week                = array( 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday' );
$price_each_day        = get_post_meta( $post->ID, '_price_each_day', true );
$price_each_day_object = json_decode( htmlspecialchars_decode( $price_each_day ) );

$tour_itinerary = get_post_meta( $post->ID, '_tour_content_itinerary', true );
$tour_address   = get_post_meta( $post->ID, '_tour_location_address', true );
$tour_lat       = get_post_meta( $post->ID, '_tour_location_lat', true );
$tour_long      = get_post_meta( $post->ID, '_tour_location_long', true );
?>
<div id="tour_booking_phys" class="panel woocommerce_options_panel">
	<p class="form-field">
		<label><?php echo esc_html__( 'Tour Code', 'travel-booking' ); ?></label>
		<input type="text" value="<?php echo $tour_code ?>" name="tour_code">
	</p>
	<p class="form-field">
		<label><?php echo esc_html__( 'Maximum ticket one booking', 'travel-booking' ); ?></label>
		<input type="number" value="<?php echo $tour_number_ticket ?>" name="tour_number_ticket">
	</p>
	<?php if ( get_option( 'show_adults_children' ) ) : ?>
		<p class="form-field">
			<label><?php echo esc_html__( 'Price of child (' . get_woocommerce_currency_symbol( get_woocommerce_currency() ) . ')', 'travel-booking' ); ?></label>
			<input type="text" value="<?php echo $price_child ?>" name="price_child">
		<p><i><?php _e( 'Default 0 is get "Price child" by percent value on setting Tour', 'travel-booking' ) ?></i></p>
		</p>
	<?php endif; ?>
	<p class="form-field">
		<label><?php echo esc_html__( 'Duration', 'travel-booking' ); ?></label>
		<input type="text" value="<?php echo $tour_duration ?>" name="tour_duration" placeholder="<?php esc_html_e( '5 Days 4 Nights', 'travel-booking' ) ?>">
	</p>
	<p class="form-field">
		<label><?php echo esc_html__( 'Start Date', 'travel-booking' ); ?></label>
		<input type="text" value="<?php echo $tour_start_date ?>" name="tour_start_date" id="tour_start_date">
	</p>
	<p class="form-field">
		<label><?php echo esc_html__( 'End Date', 'travel-booking' ); ?></label>
		<input type="text" value="<?php echo $tour_end_date ?>" name="tour_end_date" id="tour_end_date">
	</p>
	<div class="price-for-each-day">
		<h4><?php echo esc_html__( 'Days of Tour and price for each day', 'travel-booking' ); ?>
			(<?php echo get_woocommerce_currency_symbol( get_woocommerce_currency() ) ?>) </h4>
		<?php
		if ( get_option( 'show_adults_children' ) ) {
			echo '<p><i>' . __( 'Default "Price Adults" 0 will get price "Regular" or "Sale" price (if have) for it', 'travel-booking' ) . '</i></p>';
			echo '<p><i>' . __( 'Default "Price Children" 0 will get price "Price of child" for it', 'travel-booking' ) . '</i></p>';
		} else {
			echo '<p>' . __( 'Set Default 0', 'travel-booking' ) . '</p>';
		}
		?>
		<ul class="days">
			<?php
			foreach ( $a_week as $day ) {
				$checked        = '';
				$price          = 0;
				$price_children = 0;
				if ( isset( $price_each_day_object->{$day}->price ) ) {
					$checked = 'checked';
					$price   = $price_each_day_object->{$day}->price;
					if ( isset( $price_each_day_object->{$day}->price_children ) ) {
						$price_children = $price_each_day_object->{$day}->price_children;
					}
				} else {
					if ( isset( $price_each_day_object->{$day} ) ) {
						$checked = 'checked';
						$price   = $price_each_day_object->{$day};
					}
				}
				echo '<li>';
				echo '<input type="checkbox" class="day" name="' . $day . '" value="' . $day . '" ' . $checked . '>' . $day;
				if ( get_option( 'show_adults_children' ) ) {
					echo '<div class="price_adults_children">';
					echo '<div class="st_adults_children">' . __( 'Price Adults', 'travel-booking' ) . '&colon;<input class="price_day_input" type="text" name="price_' . $day . '" value="' . $price . '"></div>';
					echo '<div class="st_adults_children">' . __( 'Price Children', 'travel-booking' ) . '&colon;<input class="price_day_input_children" type="text" name="price_children_' . $day . '" value="' . $price_children . '"></div>';
					echo '</div>';
				} else {
					echo '<input class="price_day_input" type="text" name="price_' . $day . '" value="' . $price . '">';
				}
				echo '</li>';
			}
			?>
		</ul>
		<input type="hidden" name="price_each_day" value="<?php echo htmlspecialchars( $price_each_day ) ?>">
		<input type="hidden" name="date_finish_tour" value="<?php echo $date_finish_tour ?>">
	</div>
</div>

<div id="tour_data_phys" class="panel woocommerce_options_panel">
	<ul class="tabs">
		<li class="tab-link current" data-tab="tab-itinerary"><?php echo esc_html__( 'Itinerary', 'travel-booking' ); ?></li>
		<li class="tab-link" data-tab="tab-location"><?php echo esc_html__( 'Location', 'travel-booking' ); ?></li>
		<?php do_action( 'travel_tour_custom_data_title_tab' ); ?>
	</ul>

	<div id="tab-itinerary" class="tab-content current">
		<?php
		wp_editor( $tour_itinerary, 'tour_content_itinerary', array( 'editor_height' => '220px' ) );
		?>
	</div>
	<div id="tab-location" class="tab-content">
		<p class="form-field">
			<label><?php echo esc_html__( 'Address', 'travel-booking' ); ?></label>
			<input type="text" value="<?php echo $tour_address ?>" name="tour_address">
		</p>
		<p class="form-field">
			<label><?php echo esc_html__( 'Latitude', 'travel-booking' ); ?></label>
			<input type="text" value="<?php echo $tour_lat ?>" name="tour_lat">
		</p>
		<p class="form-field">
			<label><?php echo esc_html__( 'Longitude', 'travel-booking' ); ?></label>
			<input type="text" value="<?php echo $tour_long ?>" name="tour_long">
		</p>
	</div>
	<?php do_action( 'travel_tour_custom_data_content_tab' ); ?>
</div>