<?php
add_action( 'widgets_init', 'register_sidebar_tour_phys', 20 );
function register_sidebar_tour_phys() {
	$args = array(
		'name'          => __( 'Sidebar Tour', 'travel-booking' ),
		'id'            => 'sidebar-tour-phys',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	);

	register_sidebar( $args );
}
