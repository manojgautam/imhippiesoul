<?php
/**
 * Override template Woo
 */

add_filter( 'woocommerce_locate_template', 'woo_addon_plugin_template', 1, 3 );
function woo_addon_plugin_template( $template, $template_name ) {
	global $woocommerce;
	if ( $template_name == 'cart/cart.php' ) {
		$template = tb_get_file_template( 'cart.php', true, true );
	} elseif ( $template_name == 'checkout/review-order.php' ) {
		$template = tb_get_file_template( 'review-order.php', true, true );
	} elseif ( $template_name == 'order/order-details.php' ) {
		$template = tb_get_file_template( 'order-details.php', true, true );
	} elseif ( $template_name == 'order/order-details-item.php' ) {
		$template = tb_get_file_template( 'order-details-item.php', true, true );
	} elseif ( $template_name == 'emails/email-order-details.php' ) {
		$template = tb_get_file_template( 'emails/email-order-details.php', true, true );
	} elseif ( $template_name == 'emails/email-order-items.php' ) {
		$template = tb_get_file_template( 'emails/email-order-items.php', true, true );
	}

	return $template;
}

add_filter( 'template_include', 'tb_template_loader_phys', 11, 1 );
function tb_template_loader_phys( $template ) {
	$arr_find_match = array(
		'single-tour.php',
		'archive-tour.php',
		'archive-attribute.php',
		'archive-attribute-tour.php'
	);
	$find           = array( 'woocommerce.php' );
	$file           = '';

	if ( is_embed() ) {
		return $template;
	}

	if ( is_single() && get_post_type() == 'product' ) {
		if ( wc_get_product()->get_type() == 'tour_phys' ) {
			$file = 'single-tour.php';
		} else {
			$file = 'single-product.php';
		}

		$find[] = $file;
		$find[] = WC()->template_path() . $file;

	} elseif ( is_product_taxonomy() ) {

		$term = get_queried_object();

		if ( is_tax( 'product_cat' ) || is_tax( 'product_tag' ) ) {
			$file = 'taxonomy-' . $term->taxonomy . '.php';
		} else {
			if ( $term->taxonomy == 'tour_phys' ) {
				$file = 'archive-tour.php';
			} else {
				$pattern         = '/^pa_/i';
				$check_attribute = preg_match( $pattern, $term->taxonomy );
				if ( $check_attribute ) {
					$file = 'archive-attribute.php';
				} else {
					$file = 'archive-product.php';
				}
			}
		}

		$find[] = 'taxonomy-' . $term->taxonomy . '-' . $term->slug . '.php';
		$find[] = WC()->template_path() . 'taxonomy-' . $term->taxonomy . '-' . $term->slug . '.php';
		$find[] = 'taxonomy-' . $term->taxonomy . '.php';
		$find[] = WC()->template_path() . 'taxonomy-' . $term->taxonomy . '.php';
		$find[] = $file;
		$find[] = WC()->template_path() . $file;

	} elseif ( is_post_type_archive( 'product' ) || is_page( wc_get_page_id( 'shop' ) ) ) {
		$file   = 'archive-product.php';
		$find[] = $file;
		$find[] = WC()->template_path() . $file;
	}

	if ( $GLOBALS['wp_query']->get( 'is_tour' ) ) {
		$file = 'archive-tour.php';
	}

	if ( $file ) {
		$template = locate_template( array_unique( $find ) );
		if ( in_array( $file, $arr_find_match ) ) {
			if($file == 'single-tour.php') {
				$GLOBALS['wp_query']->set('is-single-tour', true);
			}
			$template = tb_get_file_template( $file );
		} else if ( ! $template || WC_TEMPLATE_DEBUG_MODE ) {
			$template = WC()->plugin_path() . '/templates/' . $file;
		}
	}

	if ( isset( $_GET['page_id'] ) && $_GET['page_id'] == get_option( Tour_Settings_Tab_Phys::$_tours_show_page_id ) ) {
		wp_safe_redirect( home_url( '?is_tour=1' ) );
	}

	return $template;
}