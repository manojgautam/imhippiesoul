<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package travelWP
 */
?>
</div><!-- #content -->

<div class="wrapper-footer<?php if ( travelwp_get_option( 'show_newsletter' ) == '1' ) {
	echo ' wrapper-footer-newsletter';
} ?>">
	<?php if ( is_active_sidebar( 'footer' ) ) : ?>
		<div class="main-top-footer">
<div class="box-icon icon-image "><a href="#top" target="_self"><img src="/wp-content/uploads/2017/04/circle_arrow.png" alt=""></a></div>
			<div class="container">
				<div class="row">
					<?php dynamic_sidebar( 'footer' ); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<div class="container wrapper-copyright">
		<div class="row">
			<div class="col-sm-6">
				<?php
				if ( travelwp_get_option( 'copyright_text' ) ) {
					echo '<div>' . travelwp_get_option( 'copyright_text' ) . '</div>';
				} else {
					echo '<div><p>' . esc_html__( 'Copyright &copy; 2017 Travel WP. All Rights Reserved.', 'travelwp' ) . '</p></div>';
				}
				?>
			</div> <!-- col-sm-3 -->
			<?php if ( is_active_sidebar( 'copyright' ) ) : ?>
				<div class="col-sm-6">
					<?php dynamic_sidebar( 'copyright' ); ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
		<div class="enqueryform">
<a href="#enquiry">General Enquiry</a>
</div>

<div class="callus">
<a href="#corporate">Corporate Enquiry</a>
</div>
</div>
<?php
if ( travelwp_get_option( 'show_newsletter' ) == '1' ) {
	$travelwp_theme_options = travelwp_get_data_themeoptions();
	$bg_image               = $travelwp_theme_options['bg_newsletter'] ['url'] ? ' style="background-image: url(' . $travelwp_theme_options['bg_newsletter'] ['url'] . ')"' : '';
	$title                  = travelwp_get_option( 'before_newsletter' ) ? '<div class="title_subtitle">' . travelwp_get_option( 'before_newsletter' ) . '</div>' : '';
	$title .= travelwp_get_option( 'title_newsletter' ) ? '<h3 class="title_primary">' . travelwp_get_option( 'title_newsletter' ) . '</h3>' : '';
	$form = travelwp_get_option( 'shortcode_newsletter' ) ? do_shortcode( travelwp_get_option( 'shortcode_newsletter' ) ) : '';
	echo '<div class="wrapper-subscribe"' . $bg_image . '>
			<div class="subscribe_shadow"></div>
			<div class="form-subscribe parallax-section stick-to-bottom form-subscribe-full-width">
				<div class="shortcode_title text-white shortcode-title-style_1 margin-bottom-3x">
				' . $title . '
				<span class="line_after_title"></span>
				</div>
				<div class="form-subscribe-form-wrap">
					<aside class="mailchimp-container">' . $form . ' </aside>
				</div>
			</div>
		</div>
 	';
}
?>

<!---------- Enquiry Forms-popup------------->
<div class="remodal" data-remodal-id="enquiry" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
<div class="enquery-form"><?php echo do_shortcode('[contact-form-7 id="837" title="General Enquire"]');?></div>
<span id=show_res> </span>
</div>
<!-- Enquiry Forms----->
<!----------Call-Us-popup------------->
<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
<div class="callus-popup"><?php echo do_shortcode('[contact-form-7 id="838" title="Call Back"]');?></div>
<span id=show_res> </span>
</div>
<!--call-us----->
<!----------Corporate-Enquiry-Us-popup------------->
<div class="remodal" data-remodal-id="corporate" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
<div class="callus-popup"><?php echo do_shortcode('[contact-form-7 id="853" title="Corporate Enquire"]');?></div>
<span id=show_res> </span>
</div>
<!--enquire-now-form----->
<!----------Corporate-Enquiry-Us-popup------------->
<div class="remodal" data-remodal-id="enqnow" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
<div class="callus-popup"><?php echo do_shortcode('[contact-form-7 id="1022" title="Enquire Now"]');?></div>
<span id=show_res> </span>
</div>
<!--enquire-now-form----->

<!----------Request-a-Brochure-popup------------->
<div class="remodal" data-remodal-id="brochure" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
<div class="callus-popup"><?php echo do_shortcode('[contact-form-7 id="1498" title="Request a Brochure"]');?></div>
<span id=show_res> </span>
</div>
<!--Request-a-Brochure-popup----->
</div>

<?php wp_footer();
?>
<script type='text/javascript' src='<?php echo bloginfo('template_directory')?>/assets/js/remodal.min.js'></script>
<script type='text/javascript' src='<?php echo bloginfo('template_directory')?>/assets/js/scrollreveal.min.js'></script>

		<script>
		
		jQuery(function() {
  jQuery('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = jQuery(this.hash);
      target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        jQuery('html, body').animate({
          scrollTop: target.offset().top-100
        }, 1000);
        return false;
      }
    }
  });
});
		 </script>

<script>

/* ==============================================
     scorll animation  -->
     =============================================== */
jQuery(document).ready(function () {
      window.sr = ScrollReveal();

      sr.reveal('.reveal-bottom', {
        origin: 'bottom',
        distance: '20px',
        duration: 1000 ,
        delay: 50,
        opacity: 1,
        scale: 0,
        easing: 'linear',
        reset: true
      });

      sr.reveal('.reveal-top', {
        origin: 'top',
        distance: '20px',
        duration: 1000 ,
        delay: 50,
        opacity: 1,
        scale: 0,
        easing: 'linear',
        reset: true
      });

      sr.reveal('.reveal-left', {
        origin: 'left',
        distance: '20px',
        duration: 1000 ,
        delay: 50,
        opacity: 1,
        scale: 0,
        easing: 'linear',
        reset: true
      });

      sr.reveal('.reveal-right', {
        origin: 'right',
        distance: '20px',
        duration: 1000 ,
        delay: 50,
        opacity: 1,
        scale: 0,
        easing: 'linear',
        reset: true
      });
        
      sr.reveal('.reveal-left-fade', {
        origin: 'left',
        distance: '20px',
        duration: 1000 ,
        delay: 50,
        opacity: 0,
        scale: 0,
        easing: 'linear',
        mobile: false
      });

      sr.reveal('.reveal-right-fade', {
        origin: 'right',
        distance: '20px',
        duration: 1000 ,
        delay: 50,
        opacity: 0,
        scale: 0,
        easing: 'linear',
        mobile: false
      });
        
      sr.reveal('.reveal-right-delay', {
        origin: 'right',
        distance: '20px',
        duration: 1000 ,
        delay: 50,
        opacity: 0,
        scale: 0,
        easing: 'linear',
        mobile: false
      }, 500);
        
      sr.reveal('.reveal-bottom-fade', {
        origin: 'bottom',
        distance: '20px',
        duration: 1000 ,
        delay: 50,
        opacity: 0,
        scale: 0,
        easing: 'linear',
        mobile: false
      });
});

</script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5a02e9f9198bd56b8c039f12/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>
</html>
