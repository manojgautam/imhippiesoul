<?php
/**
 * Template Name: Landing Page
 *
 **/
get_header(); ?>
    <div class="landing_page_temp">
    <div class="home-content container" role="main">
        <?php
        // Start the Loop.
        while (have_posts()) : the_post();
            the_content();
        endwhile;
        ?>
    </div><!-- #main-content -->
    </div>
<?php get_footer();
